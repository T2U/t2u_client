/// <reference path="../application.ts" />
'use strict';

module myApp.util {
    export class AesCBC {

        private localIQService: ng.IQService;

        constructor(private $q: ng.IQService) {
            this.localIQService = $q;
        }

        /**
         * GenerateKey (https://github.com/diafygi/webcrypto-examples)
         */
        public keyGeneration(): ng.IPromise < any > {
            var deferred = this.localIQService.defer();

            window.crypto.subtle.generateKey({
                    name: "AES-CBC",
                    length: "256", //can be  128, 192, or 256
                },
                true, //whether the key is extractable (i.e. can be used in exportKey)
                ["encrypt", "decrypt"] //can be any combination of "encrypt" and "decrypt"
            )
            .then((key: any) => {
                    //returns a key object
                    console.log(key);
                    deferred.resolve(key);
                },
                (err) => {
                    deferred.reject(err);
                }
            )

            return deferred.promise;
        }

        /**
         *RSASSA-PKCS1-v1_5 - importKey
         */
        public importKey(theKey: any): ng.IPromise < any > {
            var deferred = this.localIQService.defer();

            var jwk = { //this is an example jwk key, "raw" would be an ArrayBuffer
                kty: "oct",
                k: theKey,
                alg: "A256CBC",
                ext: true,
            };

            /*window.crypto.subtle.importKey(
          "jwk", //can be "jwk" or "raw"
          jwk,
          {   //this is the algorithm options
              name: "AES-CBC",
          },
          false, //whether the key is extractable (i.e. can be used in exportKey)
          ["encrypt", "decrypt"] //can be any combination of "encrypt" and "decrypt"
      ).then( (key: any) => {
          //returns the symmetric key
          console.log(key);
          deferred.resolve(key);
      },(err) => {
          console.error(err);
          deferred.reject(err);
                }
            )   */

            return deferred.promise;
        }

        /**
         * 
         */
        public exportKey(key: any): ng.IPromise < any > {
            var deferred = this.localIQService.defer();

            window.crypto.subtle.exportKey(
                "jwk", //can be "jwk" or "raw"
                key //extractable must be true
            ).then((keydata: any) => {
                //returns the exported key data
                console.log(keydata);

                deferred.resolve(keydata);
            }, (err) => {
                console.error(err);
                deferred.reject(err);
            })

            return deferred.promise;
        }

        /**
         * 
         */
        public encryptMessage(data: any, key: any): ng.IPromise < any > {
            var deferred = this.localIQService.defer();
            //var dataBuffer = new ArrayBuffer(data);
            window.crypto.subtle.encrypt({
                    name: "AES-CBC",
                    //Don't re-use initialization vectors!
                    //Always generate a new iv every time your encrypt!
                    iv: window.crypto.getRandomValues(new Uint8Array(16)),
                },
                key, //from generateKey or importKey above
                data //ArrayBuffer of data you want to encrypt this.str2ab(data) //ArrayBuffer of data you want to encrypt
            ).then((encrypted: any) => {
                    //returns an ArrayBuffer containing the encrypted data
                    console.log(new Uint8Array(encrypted));
                    deferred.resolve(encrypted);
                },
                (err) => {
                    //console.error(err);
                    deferred.reject(err);
                }
            )

            return deferred.promise;
        }

        /**
         * 
         */
        public decryptMessage(data: any, key: any): ng.IPromise < any > {
            var deferred = this.localIQService.defer();

            var vector = window.crypto.getRandomValues(new Uint8Array(16));

            window.crypto.subtle.decrypt({
                    name: "AES-CBC",
                    iv: vector, //The initialization vector you used to encrypt
                },
                key, //from generateKey or importKey above
                data //ArrayBuffer of the data
            ).then((decrypted: any) => {
                    //returns an ArrayBuffer containing the decrypted data
                    console.log(new Uint8Array(decrypted));

                    /*console.log(new Uint8Array(decrypted));
                console.log(this.ab2str(decrypted));
                console.log(decrypted);*/
                    deferred.resolve(decrypted);
                },
                (err) => {
                    //console.error(err);
                    console.error(err);
                    deferred.reject(err);
                }
            )

            return deferred.promise;
        }

        private ab2str(buf): any {
            return String.fromCharCode.apply(null, new Uint16Array(buf));
        }

        private str2ab(str): any {
            var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
            var bufView = new Uint16Array(buf);
            for (var i = 0, strLen = str.length; i < strLen; i++) {
                bufView[i] = str.charCodeAt(i);
            }
            return buf;
        }
    }
}