/// <reference path="../application.ts" />
/// <reference path="../services/GroupService.ts" />

'use strict';

module myApp.controllers {

	export interface ISupportedProviderControllerScope extends IControllerScope {
	}

	export class SupportedProviderController implements IController {

		//	Custom Services
		private oGroupService: myApp.services.GroupService;
    private oProviderService: myApp.services.ProviderService;
    private oProviderUserAccountService: myApp.services.ProviderUserAccountService;
		private scope: any;

    // Le context du controlleur
    private _cntx: myApp.bean.Context;
    public get cntx():myApp.bean.Context {
        return this._cntx;
    }

    /* Directive d'information sur les fournisseurs */
    public providerInformationDirective: myApp.directives.ProviderInformationModalDirective;
    
    /* Directive d'ajout d'un fournisseur à un compte */
    public addProviderAccountDirective: myApp.directives.AddProviderAccountModalDirective;

    public providerList: any;

    public myProviderList: any;

    public isLoading: boolean; /* Afficher un état de chargement */

    private route: ng.route.IRouteService;

    private registration = {
          username: "",
          email: "",
          password: "",
          confirmPassword: "",
          account: { email: "", password: "" },
          provider: { name: "" }
    };    

    constructor (private $scope: ISupportedProviderControllerScope, private oService:  myApp.services.GroupService, 
                 private _oProviderService: myApp.services.ProviderService,
                 private _oProviderUserAccountService: myApp.services.ProviderUserAccountService,
                 private $route: ng.route.IRouteService ) {

		  // Injection de la référence du controller dans le $scope
		  $scope.vm = this;

      this.scope = $scope;
      this.route = $route;

		  //  Récupération du service
		  this.oGroupService = oService;
      this.oProviderService = _oProviderService;
      this.oProviderUserAccountService = _oProviderUserAccountService;

      this.isLoading = false;

      // Récupération du context courrant
      this._cntx = myApp.bean.Context.currentContext;

      // Si authentifié on récupère les applications de l'utilisateur
      if(this._cntx != null && this._cntx.isAuth)
      {
        // Récupération des groupes de l'utilisateur
        this.oGroupService.getGroup(this._cntx.token).then( ( data ) => {
          //console.log(data);
        },
          // Error Should also return a string to be compatible with the return type
          (reason) => {return '';}
        )

        // Récupération de MES comptes de fournisseurs 
        if (this.myProviderList == null) {
          this.registration.account.email = this.cntx.account.email;
          this.getMyProviderUserAccount(this.registration);
        }
      }

        // Récupération des fournisseurs
        if(this.providerList == null)
          this.getProvider(null);
    }

    /**
     * Récupération des fournisseurs en base de données
     */
    public getProvider(parameters: any): void {
       if (this.oProviderService != null) {
        this.isLoading = true;

        this.oProviderService.getProvider(null, null).then((data) => {
                  //console.log(data);
                  this.providerList = data;

                  this.isLoading = false;
        },
        // Error Should also return a string to be compatible with the return type
        (reason) => {
            this.isLoading = false;        
            //console.log(reason.data);
            return '';
          }
        )
      }
    }

    /**
     * Récupération de MES comptes de fournisseurs en base de données
     */
    public getMyProviderUserAccount(parameters: any): void {
       if (this.oProviderUserAccountService != null) {
        this.isLoading = true;

        this.oProviderUserAccountService.getProviderUserAccount(parameters, this.cntx.token).then((data) => {
                  this.myProviderList = data;
                  this.isLoading = false;
                  // console.log(data);
        },
        // Error Should also return a string to be compatible with the return type
        (reason) => {
            this.isLoading = false;        
            return '';
          }
        )
      }
    }

    /**
     * Afficher ou Cacher la Directive courrante 'providerDirective'
     */
    public toggleModalProviderInformationDirective(provider: any): void
    {
      var jProvider = angular.fromJson(provider);

      if (this.providerInformationDirective != null  && jProvider != null) {
        if(!this.providerInformationDirective.isVisible)
        {
          this.providerInformationDirective.title = jProvider.name;
          this.providerInformationDirective.description = jProvider.description;
          this.providerInformationDirective.imageBase64 = jProvider.providerlogoBase64;

          /*console.log(jProvider);*/

          this.providerInformationDirective.show();
        }
        else
          this.providerInformationDirective.hide();
      }
    }

    /**
     * Afficher ou Cacher la Directive courrante 'AddProviderDirective'
     */
    public toggleModalAddProviderDirective(provider: any): void
    {
      var jProvider = angular.fromJson(provider);

      if (provider != null && this._cntx != null && this._cntx.isAuth) {
          // Ouvrir un formulaire de saisie pour récupérer les informations de connexion
          if (this.addProviderAccountDirective != null && jProvider != null) {
            if(!this.addProviderAccountDirective.isVisible)
            {
              this.addProviderAccountDirective.title = jProvider.name;
              this.addProviderAccountDirective.description = jProvider.description;
              this.addProviderAccountDirective.imageBase64 = jProvider.providerlogoBase64;

              this.addProviderAccountDirective.show();
            }
            else
              this.addProviderAccountDirective.hide();
          }
        }
    }

    /** 
      Cette méthode permet d'ajouter un compte fournisseur pour l'utilisateur 
    */
    private addProviderUserAccount(isvalid: boolean): void {
      if (this.cntx != null && this.cntx.isAuth && isvalid) { 
        this.registration.account.email = this.cntx.account.email;
        this.registration.provider.name = this.addProviderAccountDirective.title.toString();

        this.isLoading = true;

        this.oProviderUserAccountService.postProviderUserAccount(this.registration, null).then((data) => {
          console.log(data);
          this.isLoading = false;
          this.cleanRegistration();
          this.reloadPage();
        },
        // Error Should also return a string to be compatible with the return type
        (reason) => {
            this.isLoading = false;          
            console.log(reason.data);
            return '';
          }
        )
      }

      // Fermeture du formulaire
      this.addProviderAccountDirective.hide();
      
      //console.log(this.registration);
    }

    /** 
      Cette méthode permet de supprimer un compte fournisseur pour l'utilisateur 
    */
    private deleteProviderUserAccount(myprovider: any): void {

      //console.log(myprovider);

      if (this.cntx != null && this.cntx.isAuth && myprovider != null) { 
        this.registration.account.email = this.cntx.account.email;
        this.registration.provider.name = myprovider.provider.name;

        this.oProviderUserAccountService.deleteProviderUserAccount(this.registration, this.cntx.token).then((data) => {
          console.log(data);
          this.reloadPage();
        },
        // Error Should also return a string to be compatible with the return type
        (reason) => {
            // console.log(reason.data);
            return '';
          }
        )
      }
    }

    private reloadPage(): void {
      if(this.route != null)
        this.route.reload();
    }

    /**
     * Effacer les données temporaires
     */
    private cleanRegistration(): void {
      this.registration.username = "";
      this.registration.email = "";
      this.registration.password = "";
      this.registration.confirmPassword = "";
      this.registration.account.email = "";
      this.registration.provider.name = "";
    }    
 	}
}

// Remember to pass all the services used by the constructor of the Controller.
// WARNING view2Service but GroupService doesn't work
myApp.registerController('SupportedProviderController', ['$scope', 'groupService', 'providerService', 'providerUserAccountService' , '$route']);