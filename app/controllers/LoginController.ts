/// <reference path="../application.ts" />
/// <reference path="../services/AuthenticationService.ts" />

'use strict';

module myApp.controllers {

	export interface ILoginControllerScope extends IControllerScope {
    loginData: any;
  }

  export class LoginController implements IController {

	  //	Custom Services
		private oAuthenticationService: myApp.services.AuthenticationService;

    private scope: ILoginControllerScope;

    private location: ng.ILocationService;

     private route: ng.route.IRouteService;

    private timeoutLoginRedirect: ng.ITimeoutService;

    private loginData = {
      email: "",
      password: ""
    };

    // Alert Box
    private alertsUser: myApp.bean.Alert;

    constructor (private $scope: ILoginControllerScope, private $location: ng.ILocationService,
                 private $timeout: ng.ITimeoutService, private oService: myApp.services.AuthenticationService,
                 private $route: ng.route.IRouteService){

      // Injection de la référence du controller dans le $scope
      $scope.vm = this;

		  //  Récupération du service
      this.oAuthenticationService = oService;
      
      // Suppression du Token existant
      this.oAuthenticationService._logOut();
      
      this.scope = $scope;
      this.location = $location;
      this.timeoutLoginRedirect = $timeout;
      this.route = $route;
    }

    login(formIsValid: boolean) : void {

      if(formIsValid)
      {
        this.oAuthenticationService._login(this.loginData).then( ( data ) => {
          this.closeAlert();
          this.alertsUser = new myApp.bean.Alert('success', "User has been login successfully, you will be redicted to login page in 2 seconds.");

          this.startTimer();
          },
          // Error Should also return a string to be compatible with the return type
          (reason) => {
            this.closeAlert();
           
            var parser = new DOMParser();
            var doc = parser.parseFromString(reason.data, 'text/html');
            doc.firstChild; //this is what you're after.

            var nList: NodeList = doc.getElementsByTagName("p");

            var serverMessage: string;

            for (var n = 0; n <= nList.length; n++)
            {         
                var nNode: Node = nList.item(n);

                serverMessage = nNode.textContent;
                break;
            }

            this.alertsUser = new myApp.bean.Alert('danger', reason.status + " - [" + reason.statusText + "] " + serverMessage);

            return '';
            }
        )
      }
    }

    /**
     * Redirection automatique vers la page de l'utilisateur
     */
    private startTimer(): void {
      var timer = this.timeoutLoginRedirect(()=> {
                    this.timeoutLoginRedirect.cancel(timer);
                    this.location.path('/dashboard');
                    //this.route.reload();
                  }, 500);
    }

    /**
     * Fermer la fenêtre d'alerte
     */
    public closeAlert(): void{
       this.alertsUser = null;
    }
  }
}

// Remember to pass all the services used by the constructor of the Controller.
myApp.registerController('LoginController', ['$scope', '$location', '$timeout', 'authenticationService', '$route']);
