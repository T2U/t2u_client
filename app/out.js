var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="_all.ts" />
'use strict';
// Create and register modules
var modules = ['myApp.controllers', 'myApp.directives', 'myApp.filters', 'myApp.services'];
modules.forEach(function (module) { return angular.module(module, ['ngRoute', 'highcharts-ng', 'LocalStorageModule', 'ui.bootstrap']); });
angular.module('myApp', modules);
// Url routing $httpProvider: ng.IHttpProvider
angular.module('myApp').config(['$routeProvider',
    function routes($routeProvider) {
        $routeProvider
            .when('/', {
            templateUrl: 'views/Home.html',
            controller: 'myApp.controllers.MyController'
        })
            .when('/dashboard', {
            templateUrl: 'views/DashBoard.html',
            controller: 'myApp.controllers.DashBoardController'
        })
            .when('/login', {
            templateUrl: 'views/Login.html',
            controller: 'myApp.controllers.LoginController'
        })
            .when('/forgotpassword', {
            templateUrl: 'views/ForgotPassword.html'
        })
            .when('/signup', {
            templateUrl: 'views/Signup.html',
            controller: 'myApp.controllers.SignUpController'
        })
            .when('/supportedprovider', {
            templateUrl: 'views/ProvidersList.html',
            controller: 'myApp.controllers.SupportedProviderController'
        })
            .when('/pricing', {
            templateUrl: 'views/Pricing.html',
            controller: 'myApp.controllers.PricingController'
        })
            .when('/howitworks', {
            templateUrl: 'views/HowItWorks.html'
        })
            .when('/support', {
            templateUrl: 'views/Support.html',
            controller: 'myApp.controllers.MyController'
        })
            .when('/userprofile', {
            templateUrl: 'views/UserProfile.html',
            controller: 'myApp.controllers.UserProfileController'
        })
            .when('/privacypolicy', {
            templateUrl: 'views/PrivacyPolicy.html'
        })
            .when('/termsconditions', {
            templateUrl: 'views/TermsConditions.html'
        })
            .when('/legalnotice', {
            templateUrl: 'views/LegalNotice.html'
        })
            .otherwise({
            redirectTo: '/'
        });
    }]);
// Création du module de stockage d'informaiton
angular.module('myApp').config(function (localStorageServiceProvider) {
    localStorageServiceProvider
        .setPrefix('myApp')
        .setStorageType('sessionStorage')
        .setNotify(true, true);
});
var myApp;
(function (myApp) {
    /**
     * Register new controller.
     *
     * @param className
     * @param services
     */
    function registerController(className, services) {
        if (services === void 0) { services = []; }
        var controller = 'myApp.controllers.' + className;
        services.push(myApp.controllers[className]);
        angular.module('myApp.controllers').controller(controller, services);
    }
    myApp.registerController = registerController;
    /**
     * Register new filter.
     *
     * @param className
     * @param services
     */
    function registerFilter(className, services) {
        if (services === void 0) { services = []; }
        var filter = className.toLowerCase();
        services.push(function () { return (new myApp.filters[className]()).filter; });
        angular.module('myApp.filters').filter(filter, services);
    }
    myApp.registerFilter = registerFilter;
    /**
     * Register new directive.
     *
     * @param className
     * @param services
     */
    function registerDirective(className, services) {
        if (services === void 0) { services = []; }
        var directive = className[0].toLowerCase() + className.slice(1);
        services.push(function () { return new myApp.directives[className](); });
        //services.push(myApp.directives[className]);
        angular.module('myApp.directives').directive(directive, services);
    }
    myApp.registerDirective = registerDirective;
    /**
     * Register new service.
     *
     * @param className
     * @param services
     */
    function registerService(className, services) {
        if (services === void 0) { services = []; }
        var service = className[0].toLowerCase() + className.slice(1);
        services.push(myApp.services[className]);
        angular.module('myApp.services').service(service, services);
        /*services.push(() => new myApp.services[className]());
        angular.module('myApp.services').factory(service, services);*/
    }
    myApp.registerService = registerService;
})(myApp || (myApp = {}));
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var filters;
    (function (filters) {
        var RangeTo = (function () {
            function RangeTo() {
            }
            RangeTo.prototype.filter = function (start, end) {
                var out = [];
                for (var i = start; i < end; ++i)
                    out.push(i);
                return out;
            };
            return RangeTo;
        }());
        filters.RangeTo = RangeTo;
        var Splice = (function () {
            function Splice() {
            }
            Splice.prototype.filter = function (input, start, howMany) {
                return input.splice(start, howMany);
            };
            return Splice;
        }());
        filters.Splice = Splice;
    })(filters = myApp.filters || (myApp.filters = {}));
})(myApp || (myApp = {}));
myApp.registerFilter('RangeTo', []);
myApp.registerFilter('Splice', []);
/// <reference path='filters.ts'/> 
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var MyService = (function () {
            function MyService() {
                this.meaningOfLife = 42;
            }
            MyService.prototype.someMethod = function () {
                return 'Meaning of life is ' + this.meaningOfLife;
            };
            return MyService;
        }());
        services.MyService = MyService;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
myApp.registerService('MyService', []);
/// <reference path="../application.ts" />
'use strict';
// SRC <https://kodeyak.wordpress.com/2014/11/26/angularjs-in-typescript-services-and-http/>
// SRC <http://stackoverflow.com/questions/12827266/get-and-set-in-typescript>
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var HttpHandlerService = (function () {
            function HttpHandlerService($http) {
                this._baseUrl = "";
                this._handlerUrl = "";
                this._handlerUrlParam = "";
                this.httpService = $http;
                if (myApp.services.Configuration.instance == null)
                    var configurationObject = new myApp.services.Configuration(this.httpService, "config/config.json");
                this.updateBaseURL();
                /*if(myApp.services.Configuration.instance.SSL_enabled)
                  this._baseUrl = "https://";
                else
                  this._baseUrl = "http://";
          
                this._baseUrl = this._baseUrl + myApp.services.Configuration.instance.HostName.toString() +
                 ":" + myApp.services.Configuration.instance.Port.toString() +
                 "/" + myApp.services.Configuration.instance.WebServiceName.toString();*/
                /*console.log("HttpHandlerService:_baseUrl:" + this._baseUrl);*/
            }
            Object.defineProperty(HttpHandlerService.prototype, "httpService", {
                get: function () {
                    return this._httpService;
                },
                set: function (thehttpService) {
                    this._httpService = thehttpService;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HttpHandlerService.prototype, "handlerUrl", {
                get: function () {
                    this.updateBaseURL();
                    var tempValue;
                    if (this._handlerUrlParam == null)
                        tempValue = (this._baseUrl.toString() + this._handlerUrl.toString());
                    else
                        tempValue = (this._baseUrl.toString() + this._handlerUrl.toString() + this._handlerUrlParam.toString());
                    return (tempValue);
                },
                set: function (value) {
                    this._handlerUrl = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HttpHandlerService.prototype, "handlerUrlParam", {
                get: function () {
                    return (this._handlerUrlParam);
                },
                set: function (value) {
                    this._handlerUrlParam = value;
                },
                enumerable: true,
                configurable: true
            });
            HttpHandlerService.prototype.updateBaseURL = function () {
                if (myApp.services.Configuration.instance.SSL_enabled)
                    this._baseUrl = "https://";
                else
                    this._baseUrl = "http://";
                this._baseUrl = this._baseUrl + myApp.services.Configuration.instance.HostName.toString() +
                    ":" + myApp.services.Configuration.instance.Port.toString() +
                    myApp.services.Configuration.instance.WebServiceName.toString();
            };
            HttpHandlerService.prototype.useGetHandler = function (params) {
                var _this = this;
                var result = this._httpService.get(this.handlerUrl.toString(), params)
                    .then(function (response) { return _this.handlerResponded(response, params); });
                return result;
            };
            HttpHandlerService.prototype.usePostHandler = function (params) {
                var _this = this;
                var result = this._httpService.post(this.handlerUrl.toString(), params)
                    .then(function (response) { return _this.handlerResponded(response, params); });
                return result;
            };
            HttpHandlerService.prototype.useDeleteHandler = function (params) {
                var _this = this;
                console.log(this.handlerUrl);
                var result = this._httpService.delete(this.handlerUrl.toString(), params)
                    .then(function (response) { return _this.handlerResponded(response, params); });
                return result;
            };
            HttpHandlerService.prototype.handlerResponded = function (response, params) {
                response.data.requestParams = params;
                return response.data;
            };
            return HttpHandlerService;
        }());
        services.HttpHandlerService = HttpHandlerService; // HttpHandlerService class
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
myApp.registerService('HttpHandlerService', ['$http']);
/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />
'use strict';
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var ChartService = (function (_super) {
            __extends(ChartService, _super);
            function ChartService($http) {
                _super.call(this, $http);
                this.meaningOfLife = 9999;
                this.handlerUrl = "/extractbdd";
            }
            /* Retourner une liste de Graphiques/Chart. */
            ChartService.prototype.getChart = function (data, token) {
                this.handlerUrl = "/chart";
                if (token != null) {
                    var header = {
                        "headers": {
                            "Token": angular.toJson(token)
                        }
                    };
                }
                if (data != null)
                    this.handlerUrlParam = "?criteria=" + encodeURIComponent(angular.toJson(data));
                // console.log(this.handlerUrl);
                // console.log(this.handlerUrlParam);
                return this.useGetHandler(header);
            };
            /* Retourner une liste d'information sur la base de donnée. */
            ChartService.prototype.getExtractbdd = function () {
                this.handlerUrl = "/extractbdd";
                var config = {};
                return this.useGetHandler(config);
            };
            return ChartService;
        }(services.HttpHandlerService));
        services.ChartService = ChartService;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
myApp.registerService('ChartService', ['$http']);
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var View2Service = (function (_super) {
            __extends(View2Service, _super);
            function View2Service($http) {
                /*console.log("View2Service:HTTP:" + $http);*/
                _super.call(this, $http);
                this.$http = $http;
                /*console.log("View2Service:handlerUrl:" + this.handlerUrl.toString());*/
            }
            View2Service.prototype.simpleGetCall = function () {
                return 'View2Service ';
            };
            return View2Service;
        }(services.HttpHandlerService));
        services.View2Service = View2Service;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
myApp.registerService('View2Service', ['$http']);
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var NavBarService = (function () {
            function NavBarService(oService) {
                this.oService = oService;
                //  Récupération du service
                this.oAuthenticationService = oService;
                this.oAuthenticationService._fillAuthData();
            }
            NavBarService.prototype.getContentUrl = function () {
                if (myApp.services.AuthenticationService.authentication.isAuth) {
                    return 'views/navbarLogged.html';
                }
                else {
                    return 'views/navbarUnlogged.html';
                }
            };
            return NavBarService;
        }());
        services.NavBarService = NavBarService;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
// /!\ SUFFIX 'Directive' important /!\ http://stackoverflow.com/questions/19409017/angular-decorating-directives
myApp.registerService('NavBarService', ['authenticationService']);
/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />
'use strict';
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var GroupService = (function (_super) {
            __extends(GroupService, _super);
            function GroupService($http) {
                _super.call(this, $http);
                this.handlerUrl = "/group";
                this.cntx = myApp.bean.Context.currentContext;
            }
            /* Retourner une liste d'information sur la base de donnée. */
            GroupService.prototype.getGroup = function (token) {
                var header = {
                    "headers": {
                        "Token": angular.toJson(token)
                    }
                };
                return this.useGetHandler(header);
            };
            return GroupService;
        }(services.HttpHandlerService));
        services.GroupService = GroupService;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
myApp.registerService('GroupService', ['$http']);
/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />
'use strict';
/**
 * Service pour la récupération des informations sur les fournisseurs de service
 */
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var ProviderService = (function (_super) {
            __extends(ProviderService, _super);
            function ProviderService($http) {
                _super.call(this, $http);
                this.handlerUrl = "/provider";
                this.cntx = myApp.bean.Context.currentContext;
            }
            /* Retourner une liste d'information sur la base de donnée. */
            ProviderService.prototype.getProvider = function (data, token) {
                if (token != null) {
                    var header = {
                        "headers": {
                            "Token": angular.toJson(token)
                        }
                    };
                }
                return this.useGetHandler(header);
            };
            return ProviderService;
        }(services.HttpHandlerService));
        services.ProviderService = ProviderService;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
myApp.registerService('ProviderService', ['$http']);
/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />
'use strict';
/**
 * This class permit to get AccountLog information for target user.
 */
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var AccountLogService = (function (_super) {
            __extends(AccountLogService, _super);
            function AccountLogService($http) {
                _super.call(this, $http);
                this.handlerUrl = "/accountlog";
                this.cntx = myApp.bean.Context.currentContext;
            }
            /**
             * Retourner une liste d'information sur la base de donnée.
             */
            AccountLogService.prototype.getAccountLog = function (data, token) {
                if (token != null) {
                    var header = {
                        "headers": {
                            "Token": angular.toJson(token)
                        }
                    };
                }
                console.log(data);
                this.handlerUrlParam = "?criteria=" + encodeURIComponent(angular.toJson(data));
                console.log(this.handlerUrl);
                return this.useGetHandler(header);
            };
            return AccountLogService;
        }(services.HttpHandlerService));
        services.AccountLogService = AccountLogService;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
myApp.registerService('AccountLogService', ['$http']);
/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />
'use strict';
/**
 * Service pour la récupération des informations sur les fournisseurs de service
 */
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var ProviderUserAccountService = (function (_super) {
            __extends(ProviderUserAccountService, _super);
            function ProviderUserAccountService($http) {
                _super.call(this, $http);
                this.handlerUrl = "/provideruseraccount";
                this.cntx = myApp.bean.Context.currentContext;
            }
            /**
             * Retourner une liste d'information sur la base de donnée.
             */
            ProviderUserAccountService.prototype.getProviderUserAccount = function (data, token) {
                if (token != null) {
                    var header = {
                        "headers": {
                            "Token": angular.toJson(token)
                        }
                    };
                }
                this.handlerUrlParam = "?criteria=" + encodeURIComponent(angular.toJson(data));
                //+ ",token=" + angular.toJson(token);
                //console.log(this.handlerUrl);
                return this.useGetHandler(header);
            };
            /**
             * Ajouter un nouveau compte de frounisseur pour un utilisateur
             */
            ProviderUserAccountService.prototype.postProviderUserAccount = function (data, token) {
                if (token != null) {
                    var header = {
                        "headers": {
                            "Token": angular.toJson(token)
                        }
                    };
                }
                return this.usePostHandler(data);
            };
            /**
             * Supprimer un compte de frounisseur pour un utilisateur
             */
            ProviderUserAccountService.prototype.deleteProviderUserAccount = function (data, token) {
                if (token != null) {
                    var header = {
                        "headers": {
                            "Token": angular.toJson(token)
                        }
                    };
                }
                this.handlerUrlParam = "?criteria=" + encodeURIComponent(angular.toJson(data));
                return this.useDeleteHandler(header);
            };
            return ProviderUserAccountService;
        }(services.HttpHandlerService));
        services.ProviderUserAccountService = ProviderUserAccountService;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
myApp.registerService('ProviderUserAccountService', ['$http']);
/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />
'use strict';
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var WebCryptoService = (function (_super) {
            __extends(WebCryptoService, _super);
            function WebCryptoService($http, localStorageService, $q) {
                var _this = this;
                _super.call(this, $http);
                this.$http = $http;
                this.localStorageService = localStorageService;
                this.$q = $q;
                this.handlerUrl = "/crypto";
                this.cntx = myApp.bean.Context.currentContext;
                this._localStorageService = localStorageService;
                if (this._localStorageService.get('WebCrypto') == null ||
                    this._localStorageService.get('WebCrypto') == {}) {
                    this.oCrypto = new myApp.util.WebCrypto($q);
                    this.oCrypto.keyGeneration().then(function (key) {
                        _this._localStorageService.set('WebCrypto', _this.oCrypto);
                        console.log(_this.oCrypto);
                        if (_this.oCrypto.currentKeyPair != null)
                            _this.oCrypto.encryptMessage("TOTO test 1 2 3 4 5", _this.oCrypto.currentKeyPair.publicKey).then(function (result) {
                                console.log(new Uint8Array(result));
                                _this.oCrypto.decryptMessage(result);
                            }, function (err) {
                                console.error(err);
                            });
                    }, function (err) {
                        console.error(err);
                    });
                }
                else {
                    var webCrypto = this._localStorageService.get('WebCrypto');
                    this.oCrypto = webCrypto;
                }
            }
            return WebCryptoService;
        }(services.HttpHandlerService));
        services.WebCryptoService = WebCryptoService;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
myApp.registerService('WebCryptoService', ['$http', 'localStorageService', '$q']);
/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />
'use strict';
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var RefreshDataService = (function (_super) {
            __extends(RefreshDataService, _super);
            function RefreshDataService($http) {
                _super.call(this, $http);
                this.handlerUrl = "/refreshdata";
                this.cntx = myApp.bean.Context.currentContext;
            }
            /**
             * Retourner la liste des données rafraichie pour le ProviderUserAccount choisie.
             */
            RefreshDataService.prototype.getRefreshData = function (data, token) {
                if (token != null) {
                    var header = {
                        "headers": {
                            "Token": angular.toJson(token)
                        }
                    };
                }
                this.handlerUrlParam = "?criteria=" + encodeURIComponent(angular.toJson(data));
                console.log(this.handlerUrl);
                return this.useGetHandler(header);
            };
            return RefreshDataService;
        }(services.HttpHandlerService));
        services.RefreshDataService = RefreshDataService;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
myApp.registerService('RefreshDataService', ['$http']);
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var SubmitRequestService = (function (_super) {
            __extends(SubmitRequestService, _super);
            function SubmitRequestService($http) {
                /*console.log("View2Service:HTTP:" + $http);*/
                _super.call(this, $http);
                this.$http = $http;
                /*console.log("View2Service:handlerUrl:" + this.handlerUrl.toString());*/
            }
            SubmitRequestService.prototype.simpleGetCall = function () {
                return 'View2Service ';
            };
            return SubmitRequestService;
        }(services.HttpHandlerService));
        services.SubmitRequestService = SubmitRequestService;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
myApp.registerService('SubmitRequestService', ['$http']);
/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />
'use strict';
/*
Documentation :

- Angular Storage : https://github.com/grevory/angular-local-storage/blob/master/demo/demo-app.js
- https://github.com/borisyankov/DefinitelyTyped/blob/master/angular-local-storage/angular-local-storage.d.ts
- Auth template example : https://github.com/tjoudeh/AngularJSAuthentication

*/
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var AuthenticationService = (function (_super) {
            __extends(AuthenticationService, _super);
            function AuthenticationService($http, $q, localStorageService) {
                _super.call(this, $http);
                this.$http = $http;
                this.$q = $q;
                this.localStorageService = localStorageService;
                /*static set authentication(value: _authentication) {
                    this._authentication = value;
                }*/
                this.token = {
                    data: String,
                    issuedUtc: String,
                    expiresUtc: String,
                    email: String
                };
                this.localIQService = $q;
                //console.log("AuthenticationService:HTTP:" + String($http != null));
                //console.log("AuthenticationService:localStorageService:" + String(localStorageService != null));
                this.authServiceFactory = {};
                this.oStorageService = localStorageService;
            }
            Object.defineProperty(AuthenticationService, "authentication", {
                get: function () {
                    return this._authentication;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AuthenticationService, "currentToken", {
                get: function () {
                    return this._currentToken;
                },
                enumerable: true,
                configurable: true
            });
            /**
             *   Sauvegarder un nouveau Account
             */
            AuthenticationService.prototype._saveRegistration = function (registration) {
                this._logOut();
                this.handlerUrl = "/account/register";
                return this.usePostHandler(registration);
            };
            /**
             *   Deconnexion de l'utilisateur entraine la suppression du Token.
             */
            AuthenticationService.prototype._logOut = function () {
                this.oStorageService.remove('authorizationData');
                myApp.services.AuthenticationService._authentication.isAuth = false;
                myApp.services.AuthenticationService._authentication.email = "";
                // Effacer le contexte courant 
                myApp.bean.Context.clear();
            };
            /**
             *    Tentative de connexion de l'utilisateur et récupération d'un Token pour réaliser les appels au web service
             */
            AuthenticationService.prototype._login = function (loginData) {
                var _this = this;
                this.handlerUrl = "/token";
                var deferred = this.localIQService.defer();
                this.usePostHandler(loginData).then(function (response) {
                    //console.log(response);
                    //var result = angular.fromJson(response);
                    //console.log(result);
                    for (var i = 0; i < response.length; i++) {
                        _this.token.data = response[i].data;
                        _this.token.issuedUtc = response[i].issuedUtc;
                        _this.token.expiresUtc = response[i].expiresUtc;
                        _this.token.email = loginData.email;
                        break;
                    }
                    //var result = angular.fromJson(response);
                    //console.log(this.token);
                    _this.oStorageService.set('authorizationData', _this.token);
                    // myApp.services.AuthenticationService.
                    //myApp.services.AuthenticationService._authentication.isAuth = true;
                    //myApp.services.AuthenticationService._authentication.email = loginData.email;
                    //console.log(myApp.services.AuthenticationService.authentication);
                    _this._fillAuthData();
                    deferred.resolve(response);
                }, 
                // Error Should also return a string to be compatible with the return type
                function (reason) {
                    _this._logOut();
                    deferred.reject(reason);
                });
                return deferred.promise;
            };
            /**
             * Refresh static variable.
             */
            AuthenticationService.prototype._fillAuthData = function () {
                var tokenData = this.oStorageService.get('authorizationData');
                //console.log(tokenData);
                if (tokenData) {
                    myApp.services.AuthenticationService._authentication.isAuth = true;
                    myApp.services.AuthenticationService._authentication.email = tokenData.email;
                    myApp.services.AuthenticationService._currentToken = new myApp.bean.Token(tokenData.data, tokenData.issuedUtc, tokenData.expiresUtc);
                    this.cntx = new myApp.bean.Context(myApp.services.AuthenticationService._currentToken, true, tokenData.email);
                }
                //console.log(myApp.services.AuthenticationService.authentication);
            };
            AuthenticationService._authentication = {
                isAuth: false,
                email: ""
            };
            return AuthenticationService;
        }(services.HttpHandlerService));
        services.AuthenticationService = AuthenticationService;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
// 
myApp.registerService('AuthenticationService', ['$http', '$q', 'localStorageService']);
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var services;
    (function (services) {
        var Configuration = (function () {
            function Configuration(thehttpService, pFicConf) {
                var _this = this;
                this.thehttpService = thehttpService;
                this.pFicConf = pFicConf;
                /* Activation du HTTPS */
                this._SSL_enabled = false;
                /* Port pour les appels HTTP */
                this._port = 8000;
                /* Nom du service pour les appels HTTP */
                this._webServiceName = "";
                /* Nom de l'hote pour les appels HTTP */
                this._hostName = "";
                /* Nom de l'hote pour les appels BI */
                this._hostNameBI = "";
                var configFilePath = pFicConf || 'config/config.json';
                thehttpService.get(configFilePath.toString()).then(function (response) {
                    // console.log(response);
                    _this.loadConfigurationFromString(response.data);
                });
                myApp.services.Configuration.instance = this;
            }
            Object.defineProperty(Configuration, "instance", {
                get: function () {
                    return this._instance;
                },
                set: function (value) {
                    this._instance = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Configuration.prototype, "SSL_enabled", {
                get: function () {
                    return this._SSL_enabled;
                },
                set: function (value) {
                    this._SSL_enabled = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Configuration.prototype, "Port", {
                get: function () {
                    return this._port;
                },
                set: function (value) {
                    this._port = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Configuration.prototype, "WebServiceName", {
                get: function () {
                    return this._webServiceName;
                },
                set: function (value) {
                    this._webServiceName = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Configuration.prototype, "HostName", {
                get: function () {
                    return this._hostName;
                },
                set: function (value) {
                    this._hostName = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Configuration.prototype, "HostNameBI", {
                get: function () {
                    return this._hostNameBI;
                },
                set: function (value) {
                    this._hostNameBI = value;
                },
                enumerable: true,
                configurable: true
            });
            /*
                Charge la configuration du client web depuis un String JSON
            */
            Configuration.prototype.loadConfigurationFromString = function (jSonData) {
                //console.log(jSonData);
                var result = jSonData;
                var configObject;
                result = angular.fromJson(result);
                // #tag=configuration CHANGE HERE TO LOAD DEVELOPPMENT OR PRODUCTION CONFIGURATION
                //configObject = result.configuration_dev;
                if (location.protocol == "http")
                    configObject = result.configuration_prod;
                else
                    configObject = result.configuration_prod_secure;
                this._hostName = configObject.hote;
                this._port = configObject.port;
                this._webServiceName = configObject.nomservice;
                this.SSL_enabled = configObject.ssl_enabled;
                this._hostNameBI = configObject.bi_server;
            };
            return Configuration;
        }());
        services.Configuration = Configuration;
    })(services = myApp.services || (myApp.services = {}));
})(myApp || (myApp = {}));
myApp.registerService('Configuration', ['$http', String]);
/// <reference path='MyService.ts'/>
/// <reference path='ChartService.ts'/>
/// <reference path='View2Service.ts'/>
/// <reference path='NavBarService.ts'/>
/// <reference path='GroupService.ts'/>
/// <reference path='ProviderService.ts'/>
/// <reference path='AccountLogService.ts'/>
/// <reference path='ProviderUserAccountService.ts'/>
/// <reference path='WebCryptoService.ts'/>
/// <reference path='RefreshDataService.ts'/>
/// <reference path='SubmitRequestService'/>
/// <reference path='HttpHandlerService.ts'/>
/// <reference path='AuthenticationService.ts'/>
/// <reference path='../util/Configuration.ts'/> 
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var controllers;
    (function (controllers) {
        var MyController = (function () {
            function MyController($scope, myService, _SubmitRequestService) {
                this.$scope = $scope;
                this.myService = myService;
                this._SubmitRequestService = _SubmitRequestService;
                // Injection de la référence du controller dans le $scope
                $scope.vm = this;
                // Récupération du context courrant	
                this._cntx = myApp.bean.Context.currentContext;
                this.oSubmitRequestService = _SubmitRequestService;
            }
            Object.defineProperty(MyController.prototype, "cntx", {
                get: function () {
                    return this._cntx;
                },
                enumerable: true,
                configurable: true
            });
            return MyController;
        }());
        controllers.MyController = MyController;
    })(controllers = myApp.controllers || (myApp.controllers = {}));
})(myApp || (myApp = {}));
// Remember to pass all the services used by the constructor of the Controller.
myApp.registerController('MyController', ['$scope', 'myService', 'submitRequestService']);
/// <reference path="../application.ts" />
// /// <reference path="../services/ChartService.ts" />
'use strict';
var myApp;
(function (myApp) {
    var controllers;
    (function (controllers) {
        var DashBoardController = (function () {
            function DashBoardController($scope, oService) {
                this.$scope = $scope;
                this.oService = oService;
                // Liste des Charts
                this.chartObjectArray = new Array();
                this.chart = {
                    account: { email: '' }
                };
                // Injection de la référence du controller dans le $scope
                $scope.vm = this;
                //  Récupération du service
                this.oChartService = oService;
                this.scope = $scope;
                // Récupération du context courrant
                this._cntx = myApp.bean.Context.currentContext;
                // Chargement des Graphiques
                this.loadingAllCharts();
                // Exemple :
                this.pieChartExample = myApp.bean.ChartObject.getNewChart('pie');
                this.singleLineSeriesChartExample = myApp.bean.ChartObject.getNewChart('single_line_series');
                this.basicAreaChartExample = myApp.bean.ChartObject.getNewChart('basic_area');
            }
            Object.defineProperty(DashBoardController.prototype, "cntx", {
                get: function () {
                    return this._cntx;
                },
                enumerable: true,
                configurable: true
            });
            /**
             * Chargement de tous les Charts du Client
             */
            DashBoardController.prototype.loadingAllCharts = function () {
                var _this = this;
                var oChart;
                if (this.oChartService != null) {
                    // Récupération des charts du Client connecté
                    this.chart.account.email = this.cntx.account.email;
                    this.oChartService.getChart(this.chart, this.cntx.token).then(function (data) {
                        //console.log(data); 
                        _this.chartResult = data;
                        // Pour chaque Chart retourné
                        for (var _i = 0, _a = _this.chartResult; _i < _a.length; _i++) {
                            var val = _a[_i];
                            oChart = myApp.bean.ChartObject.getNewChart(myApp.bean.ChartObject.getRealChartName(val.chartType));
                            /* Tableau HighChartJS */
                            oChart.chartConfig.subtitle.text = val.description;
                            for (var _b = 0, _c = val.enteteDocumentList; _b < _c.length; _b++) {
                                var val2 = _c[_b];
                                // Ajouter les données à oChart depuis val2
                                _this.addSeries2(val2, oChart);
                                // Ajouter le Chart a la liste
                                _this.chartObjectArray.push(oChart);
                            }
                        }
                    }, function (reason) {
                        //console.log(reason);
                        return '';
                    });
                }
            };
            DashBoardController.prototype.simpleGetCall = function (lsucces) {
                var _this = this;
                this.oChartService.getExtractbdd().then(function (data) {
                    _this.addSeries(data);
                });
            };
            /*
              Ajoute une série au tableau HightChartJS
            */
            DashBoardController.prototype.addSeries = function (jSonData) {
                // Tableau de variable
                var rnd = [];
                /*if( jSonData.length != 0){*/
                var empty = "";
                var jString = jSonData;
                if (jSonData === empty) {
                    for (var i = 0; i < 10; i++) {
                        rnd.push(Math.floor(Math.random() * 20) + 1);
                    }
                }
                else {
                    // Récupération de la liste des événements dans la variable {jSonData}
                    var result = angular.fromJson(jString);
                    // alert(result);
                    // Récupération de la durée de chaque événement et l'ajouter au HighChart
                    for (var i = 0; i < result.length; i++) {
                        rnd.push(result[i].montant);
                    }
                }
                this.pieChartExample.chartConfig.series.push({ data: rnd });
                this.singleLineSeriesChartExample.chartConfig.series.push({ data: rnd });
            };
            /**
             * Version 2 avec un Chart
             */
            DashBoardController.prototype.addSeries2 = function (enteteDocument, oChart) {
                // Tableau de variable
                var rnd = [];
                /*if( jSonData.length != 0){*/
                var empty = "";
                if (enteteDocument != null) {
                    for (var _i = 0, _a = enteteDocument.lineDocumentList; _i < _a.length; _i++) {
                        var val = _a[_i];
                        rnd.push(val.valeur01);
                    }
                }
                // Ajouter la Série au Chart
                oChart.chartConfig.series.push({ data: rnd, name: enteteDocument.description + "," + enteteDocument.updatedString });
            };
            /**
             * Change le Mode d'un Chart
             */
            DashBoardController.prototype.swapChartType = function (oChartObject) {
                //console.log(oChartObject);
                if (oChartObject.chartType == 'bar') {
                    if (oChartObject.chartConfig.options.chart.type === 'line') {
                        oChartObject.chartConfig.options.chart.type = 'bar';
                    }
                    else {
                        oChartObject.chartConfig.options.chart.type = 'line';
                    }
                }
            };
            return DashBoardController;
        }());
        controllers.DashBoardController = DashBoardController;
    })(controllers = myApp.controllers || (myApp.controllers = {}));
})(myApp || (myApp = {}));
// Remember to pass all the services used by the constructor of the Controller.
// WARNING chartService but ChartService doesn't work
myApp.registerController('DashBoardController', ['$scope', 'chartService']);
/// <reference path="../application.ts" />
/// <reference path="../services/View2Service.ts" />
'use strict';
var myApp;
(function (myApp) {
    var controllers;
    (function (controllers) {
        var View2Controller = (function () {
            function View2Controller($scope, oService) {
                this.$scope = $scope;
                this.oService = oService;
                // Injection de la référence du controller dans le $scope
                $scope.vm = this;
                //  Récupération du service
                this.oView2Service = oService;
                this.scope = $scope;
                this.scope.message = "TEST "; //oService.simpleGetCall();
                this.isAuth = myApp.services.AuthenticationService.authentication.isAuth;
            }
            View2Controller.prototype.swap = function (item) {
                if (item != null) {
                    var id = item.currentTarget.id; // 345
                    $('#' + id).find('.card').addClass('flipped');
                    return false;
                }
            };
            View2Controller.prototype.revertswap = function (item) {
                if (item != null) {
                    var id = item.currentTarget.id; // 345
                    $('#' + id).find('.card').removeClass('flipped');
                    return false;
                }
            };
            return View2Controller;
        }());
        controllers.View2Controller = View2Controller;
    })(controllers = myApp.controllers || (myApp.controllers = {}));
})(myApp || (myApp = {}));
// Remember to pass all the services used by the constructor of the Controller.
// WARNING view2Service but View2Service doesn't work
myApp.registerController('View2Controller', ['$scope', 'view2Service']);
/// <reference path="../application.ts" />
/// <reference path="../services/AuthenticationService.ts" />
'use strict';
var myApp;
(function (myApp) {
    var controllers;
    (function (controllers) {
        var LoginController = (function () {
            function LoginController($scope, $location, $timeout, oService, $route) {
                this.$scope = $scope;
                this.$location = $location;
                this.$timeout = $timeout;
                this.oService = oService;
                this.$route = $route;
                this.loginData = {
                    email: "",
                    password: ""
                };
                // Injection de la référence du controller dans le $scope
                $scope.vm = this;
                //  Récupération du service
                this.oAuthenticationService = oService;
                // Suppression du Token existant
                this.oAuthenticationService._logOut();
                this.scope = $scope;
                this.location = $location;
                this.timeoutLoginRedirect = $timeout;
                this.route = $route;
            }
            LoginController.prototype.login = function (formIsValid) {
                var _this = this;
                if (formIsValid) {
                    this.oAuthenticationService._login(this.loginData).then(function (data) {
                        _this.closeAlert();
                        _this.alertsUser = new myApp.bean.Alert('success', "User has been login successfully, you will be redicted to login page in 2 seconds.");
                        _this.startTimer();
                    }, 
                    // Error Should also return a string to be compatible with the return type
                    function (reason) {
                        _this.closeAlert();
                        var parser = new DOMParser();
                        var doc = parser.parseFromString(reason.data, 'text/html');
                        doc.firstChild; //this is what you're after.
                        var nList = doc.getElementsByTagName("p");
                        var serverMessage;
                        for (var n = 0; n <= nList.length; n++) {
                            var nNode = nList.item(n);
                            serverMessage = nNode.textContent;
                            break;
                        }
                        _this.alertsUser = new myApp.bean.Alert('danger', reason.status + " - [" + reason.statusText + "] " + serverMessage);
                        return '';
                    });
                }
            };
            /**
             * Redirection automatique vers la page de l'utilisateur
             */
            LoginController.prototype.startTimer = function () {
                var _this = this;
                var timer = this.timeoutLoginRedirect(function () {
                    _this.timeoutLoginRedirect.cancel(timer);
                    _this.location.path('/dashboard');
                    //this.route.reload();
                }, 500);
            };
            /**
             * Fermer la fenêtre d'alerte
             */
            LoginController.prototype.closeAlert = function () {
                this.alertsUser = null;
            };
            return LoginController;
        }());
        controllers.LoginController = LoginController;
    })(controllers = myApp.controllers || (myApp.controllers = {}));
})(myApp || (myApp = {}));
// Remember to pass all the services used by the constructor of the Controller.
myApp.registerController('LoginController', ['$scope', '$location', '$timeout', 'authenticationService', '$route']);
/// <reference path="../application.ts" />
/// <reference path="../services/AuthenticationService.ts" />
'use strict';
var myApp;
(function (myApp) {
    var controllers;
    (function (controllers) {
        var SignUpController = (function () {
            function SignUpController($scope, $location, $timeout, oService) {
                this.$scope = $scope;
                this.$location = $location;
                this.$timeout = $timeout;
                this.oService = oService;
                this.registration = {
                    username: "",
                    email: "",
                    password: "",
                    confirmPassword: ""
                };
                /* TODO : Cet Objet ne sert qu'à mapper les noms avec les nom des beans sur le serveur userName -> EMAIL*/
                this.registrationBinding = {
                    username: "",
                    email: "",
                    password: ""
                };
                // Injection de la référence du controller dans le $scope
                $scope.vm = this;
                //  Récupération du service
                this.oAuthenticationService = oService;
                this.scope = $scope;
                this.location = $location;
                this.timeoutLoginRedirect = $timeout;
            }
            /**
             * Enregistrement d'un nouvel utilisateur depuis l'objet registration
             **/
            SignUpController.prototype.signUp = function (formIsValid) {
                var _this = this;
                if (formIsValid) {
                    this.registrationBinding.username = this.registration.username;
                    this.registrationBinding.email = this.registration.email;
                    this.registrationBinding.password = this.registration.password;
                    //console.log(this.registrationBinding);
                    this.oAuthenticationService._saveRegistration(this.registrationBinding).then(function (data) {
                        _this.closeAlert();
                        _this.alertsUser = new myApp.bean.Alert('success', "User has been registered successfully, you will be redicted to login page in 2 seconds.");
                        _this.startTimer();
                    }, 
                    // Error Should also return a string to be compatible with the return type
                    function (reason) {
                        _this.closeAlert();
                        var parser = new DOMParser();
                        var doc = parser.parseFromString(reason.data, 'text/html');
                        doc.firstChild; //this is what you're after.
                        var nList = doc.getElementsByTagName("p");
                        var serverMessage;
                        for (var n = 0; n <= nList.length; n++) {
                            var nNode = nList.item(n);
                            serverMessage = nNode.textContent;
                            break;
                        }
                        _this.alertsUser = new myApp.bean.Alert('danger', reason.status + " - [" + reason.statusText + "] " + serverMessage);
                        return '';
                    });
                }
            };
            /**
             * Redirection automatique vers la page de connection
             **/
            SignUpController.prototype.startTimer = function () {
                var _this = this;
                var timer = this.timeoutLoginRedirect(function () {
                    _this.timeoutLoginRedirect.cancel(timer);
                    _this.location.path('/login');
                }, 2200);
            };
            /**
             * Fermer la fenêtre d'alerte
             */
            SignUpController.prototype.closeAlert = function () {
                this.alertsUser = null;
            };
            return SignUpController;
        }());
        controllers.SignUpController = SignUpController;
    })(controllers = myApp.controllers || (myApp.controllers = {}));
})(myApp || (myApp = {}));
// Remember to pass all the services used by the constructor of the Controller.
// WARNING AuthenticationService but authenticationService doesn't work
myApp.registerController('SignUpController', ['$scope', '$location', '$timeout', 'authenticationService']);
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var controllers;
    (function (controllers) {
        var NavBarController = (function () {
            function NavBarController($scope, oNavService) {
                this.$scope = $scope;
                this.oNavService = oNavService;
                // Injection de la référence du controller dans le $scope
                $scope.vm = this;
                this.scope = $scope;
                // Récupération du service de naviguation
                this.oNavBarService = oNavService;
                // Récupération du context courrant    
                this._cntx = myApp.bean.Context.currentContext;
                this._config = myApp.services.Configuration.instance;
                //if(myApp.services.Configuration.instance == null)
                //var configurationObject = new myApp.services.Configuration(this.httpService, "config/config.json");
                console.log(this._config);
            }
            Object.defineProperty(NavBarController.prototype, "config", {
                get: function () {
                    return this._config;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(NavBarController.prototype, "cntx", {
                get: function () {
                    return this._cntx;
                },
                enumerable: true,
                configurable: true
            });
            /**
             * Retourner l'url pour choisir une vue connecté / deconnecté
             */
            NavBarController.prototype.getContentUrl = function () {
                return this.oNavBarService.getContentUrl();
            };
            return NavBarController;
        }());
        controllers.NavBarController = NavBarController;
    })(controllers = myApp.controllers || (myApp.controllers = {}));
})(myApp || (myApp = {}));
// Remember to pass all the services used by the constructor of the Controller.
myApp.registerController('NavBarController', ['$scope', 'navBarService']);
/// <reference path="../application.ts" />
/// <reference path="../services/View2Service.ts" />
'use strict';
var myApp;
(function (myApp) {
    var controllers;
    (function (controllers) {
        var PricingController = (function () {
            function PricingController($scope) {
                this.$scope = $scope;
                // Injection de la référence du controller dans le $scope
                $scope.vm = this;
                this.scope = $scope;
            }
            PricingController.prototype.test = function (item) {
                return true;
            };
            return PricingController;
        }());
        controllers.PricingController = PricingController;
    })(controllers = myApp.controllers || (myApp.controllers = {}));
})(myApp || (myApp = {}));
// Remember to pass all the services used by the constructor of the Controller.
// WARNING PricingService but PricingService doesn't work
myApp.registerController('PricingController', ['$scope']);
/// <reference path="../application.ts" />
/// <reference path="../services/GroupService.ts" />
'use strict';
var myApp;
(function (myApp) {
    var controllers;
    (function (controllers) {
        var SupportedProviderController = (function () {
            function SupportedProviderController($scope, oService, _oProviderService, _oProviderUserAccountService, $route) {
                this.$scope = $scope;
                this.oService = oService;
                this._oProviderService = _oProviderService;
                this._oProviderUserAccountService = _oProviderUserAccountService;
                this.$route = $route;
                this.registration = {
                    username: "",
                    email: "",
                    password: "",
                    confirmPassword: "",
                    account: { email: "", password: "" },
                    provider: { name: "" }
                };
                // Injection de la référence du controller dans le $scope
                $scope.vm = this;
                this.scope = $scope;
                this.route = $route;
                //  Récupération du service
                this.oGroupService = oService;
                this.oProviderService = _oProviderService;
                this.oProviderUserAccountService = _oProviderUserAccountService;
                this.isLoading = false;
                // Récupération du context courrant
                this._cntx = myApp.bean.Context.currentContext;
                // Si authentifié on récupère les applications de l'utilisateur
                if (this._cntx != null && this._cntx.isAuth) {
                    // Récupération des groupes de l'utilisateur
                    this.oGroupService.getGroup(this._cntx.token).then(function (data) {
                        //console.log(data);
                    }, 
                    // Error Should also return a string to be compatible with the return type
                    function (reason) { return ''; });
                    // Récupération de MES comptes de fournisseurs 
                    if (this.myProviderList == null) {
                        this.registration.account.email = this.cntx.account.email;
                        this.getMyProviderUserAccount(this.registration);
                    }
                }
                // Récupération des fournisseurs
                if (this.providerList == null)
                    this.getProvider(null);
            }
            Object.defineProperty(SupportedProviderController.prototype, "cntx", {
                get: function () {
                    return this._cntx;
                },
                enumerable: true,
                configurable: true
            });
            /**
             * Récupération des fournisseurs en base de données
             */
            SupportedProviderController.prototype.getProvider = function (parameters) {
                var _this = this;
                if (this.oProviderService != null) {
                    this.isLoading = true;
                    this.oProviderService.getProvider(null, null).then(function (data) {
                        //console.log(data);
                        _this.providerList = data;
                        _this.isLoading = false;
                    }, 
                    // Error Should also return a string to be compatible with the return type
                    function (reason) {
                        _this.isLoading = false;
                        //console.log(reason.data);
                        return '';
                    });
                }
            };
            /**
             * Récupération de MES comptes de fournisseurs en base de données
             */
            SupportedProviderController.prototype.getMyProviderUserAccount = function (parameters) {
                var _this = this;
                if (this.oProviderUserAccountService != null) {
                    this.isLoading = true;
                    this.oProviderUserAccountService.getProviderUserAccount(parameters, this.cntx.token).then(function (data) {
                        _this.myProviderList = data;
                        _this.isLoading = false;
                        // console.log(data);
                    }, 
                    // Error Should also return a string to be compatible with the return type
                    function (reason) {
                        _this.isLoading = false;
                        return '';
                    });
                }
            };
            /**
             * Afficher ou Cacher la Directive courrante 'providerDirective'
             */
            SupportedProviderController.prototype.toggleModalProviderInformationDirective = function (provider) {
                var jProvider = angular.fromJson(provider);
                if (this.providerInformationDirective != null && jProvider != null) {
                    if (!this.providerInformationDirective.isVisible) {
                        this.providerInformationDirective.title = jProvider.name;
                        this.providerInformationDirective.description = jProvider.description;
                        this.providerInformationDirective.imageBase64 = jProvider.providerlogoBase64;
                        /*console.log(jProvider);*/
                        this.providerInformationDirective.show();
                    }
                    else
                        this.providerInformationDirective.hide();
                }
            };
            /**
             * Afficher ou Cacher la Directive courrante 'AddProviderDirective'
             */
            SupportedProviderController.prototype.toggleModalAddProviderDirective = function (provider) {
                var jProvider = angular.fromJson(provider);
                if (provider != null && this._cntx != null && this._cntx.isAuth) {
                    // Ouvrir un formulaire de saisie pour récupérer les informations de connexion
                    if (this.addProviderAccountDirective != null && jProvider != null) {
                        if (!this.addProviderAccountDirective.isVisible) {
                            this.addProviderAccountDirective.title = jProvider.name;
                            this.addProviderAccountDirective.description = jProvider.description;
                            this.addProviderAccountDirective.imageBase64 = jProvider.providerlogoBase64;
                            this.addProviderAccountDirective.show();
                        }
                        else
                            this.addProviderAccountDirective.hide();
                    }
                }
            };
            /**
              Cette méthode permet d'ajouter un compte fournisseur pour l'utilisateur
            */
            SupportedProviderController.prototype.addProviderUserAccount = function (isvalid) {
                var _this = this;
                if (this.cntx != null && this.cntx.isAuth && isvalid) {
                    this.registration.account.email = this.cntx.account.email;
                    this.registration.provider.name = this.addProviderAccountDirective.title.toString();
                    this.isLoading = true;
                    this.oProviderUserAccountService.postProviderUserAccount(this.registration, null).then(function (data) {
                        console.log(data);
                        _this.isLoading = false;
                        _this.cleanRegistration();
                        _this.reloadPage();
                    }, 
                    // Error Should also return a string to be compatible with the return type
                    function (reason) {
                        _this.isLoading = false;
                        console.log(reason.data);
                        return '';
                    });
                }
                // Fermeture du formulaire
                this.addProviderAccountDirective.hide();
                //console.log(this.registration);
            };
            /**
              Cette méthode permet de supprimer un compte fournisseur pour l'utilisateur
            */
            SupportedProviderController.prototype.deleteProviderUserAccount = function (myprovider) {
                //console.log(myprovider);
                var _this = this;
                if (this.cntx != null && this.cntx.isAuth && myprovider != null) {
                    this.registration.account.email = this.cntx.account.email;
                    this.registration.provider.name = myprovider.provider.name;
                    this.oProviderUserAccountService.deleteProviderUserAccount(this.registration, this.cntx.token).then(function (data) {
                        console.log(data);
                        _this.reloadPage();
                    }, 
                    // Error Should also return a string to be compatible with the return type
                    function (reason) {
                        // console.log(reason.data);
                        return '';
                    });
                }
            };
            SupportedProviderController.prototype.reloadPage = function () {
                if (this.route != null)
                    this.route.reload();
            };
            /**
             * Effacer les données temporaires
             */
            SupportedProviderController.prototype.cleanRegistration = function () {
                this.registration.username = "";
                this.registration.email = "";
                this.registration.password = "";
                this.registration.confirmPassword = "";
                this.registration.account.email = "";
                this.registration.provider.name = "";
            };
            return SupportedProviderController;
        }());
        controllers.SupportedProviderController = SupportedProviderController;
    })(controllers = myApp.controllers || (myApp.controllers = {}));
})(myApp || (myApp = {}));
// Remember to pass all the services used by the constructor of the Controller.
// WARNING view2Service but GroupService doesn't work
myApp.registerController('SupportedProviderController', ['$scope', 'groupService', 'providerService', 'providerUserAccountService', '$route']);
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var controllers;
    (function (controllers) {
        var UserProfileController = (function () {
            function UserProfileController($scope, _oProviderUserAccountService, $route, _oAccountLogService, _RefreshDataService, _oChartService) {
                this.$scope = $scope;
                this._oProviderUserAccountService = _oProviderUserAccountService;
                this.$route = $route;
                this._oAccountLogService = _oAccountLogService;
                this._RefreshDataService = _RefreshDataService;
                this._oChartService = _oChartService;
                this.myAccountsLog = {
                    account: { email: "", username: "" }
                };
                this.registration = {
                    id_provideruseraccount: 0,
                    username: "",
                    email: "",
                    password: "",
                    confirmPassword: "",
                    account: { email: "", password: "" },
                    provider: { name: "" }
                };
                this.chart = {
                    account: { email: '' }
                };
                // Injection de la référence du controller dans le $scope
                $scope.vm = this;
                this.scope = $scope;
                this.route = $route;
                // Récupération du context courrant
                this._cntx = myApp.bean.Context.currentContext;
                this.oProviderUserAccountService = _oProviderUserAccountService;
                this.oAccountLogService = _oAccountLogService;
                this.oChartService = _oChartService;
                this.alertsUserList = new Array();
                // Récupération de MES logs
                if (this.myAccountLogList == null) {
                    this.myAccountsLog.account.email = this.cntx.account.email;
                    this.getMyAccountLog(this.myAccountsLog);
                }
                // Chargement des Graphiques
                this.loadingAllCharts();
            }
            Object.defineProperty(UserProfileController.prototype, "cntx", {
                get: function () {
                    return this._cntx;
                },
                enumerable: true,
                configurable: true
            });
            /**
             * Chargement de tous les Charts du Client
             */
            UserProfileController.prototype.loadingAllCharts = function () {
                var _this = this;
                var oChart;
                if (this.oChartService != null) {
                    // Récupération des charts du Client connecté
                    /*this.chart.account.email = this.cntx.account.email;*/
                    this.oChartService.getChart(this.chart, this.cntx.token).then(function (data) {
                        console.log(data);
                        _this.chartResult = data;
                    }, function (reason) {
                        var parser = new DOMParser();
                        var doc = parser.parseFromString(reason.data, 'text/html');
                        doc.firstChild; //this is what you're after.
                        var nList = doc.getElementsByTagName("p");
                        var serverMessage;
                        for (var n = 0; n <= nList.length; n++) {
                            var nNode = nList.item(n);
                            serverMessage = nNode.textContent;
                            break;
                        }
                        _this.alertsUserList.push(new myApp.bean.Alert('danger', reason.status + " - [" + reason.statusText + "] " + serverMessage));
                        return '';
                    });
                }
            };
            /**
             * Récupération de MES comptes de fournisseurs en base de données
             */
            UserProfileController.prototype.getMyAccountLog = function (parameters) {
                var _this = this;
                if (this.oAccountLogService != null) {
                    this.oAccountLogService.getAccountLog(parameters, this.cntx.token).then(function (data) {
                        _this.myAccountLogList = data;
                        //console.log(data);
                    }, 
                    // Error Should also return a string to be compatible with the return type
                    function (reason) {
                        var parser = new DOMParser();
                        var doc = parser.parseFromString(reason.data, 'text/html');
                        doc.firstChild; //this is what you're after.
                        var nList = doc.getElementsByTagName("p");
                        var serverMessage;
                        for (var n = 0; n <= nList.length; n++) {
                            var nNode = nList.item(n);
                            serverMessage = nNode.textContent;
                            break;
                        }
                        _this.alertsUserList.push(new myApp.bean.Alert('danger', reason.status + " - [" + reason.statusText + "] " + serverMessage));
                        return '';
                    });
                }
            };
            /**
             *
             */
            UserProfileController.prototype.getRefreshData = function () {
                // console.log(this.mainPassword);
                this.mainPassword = null;
                /*
               this.registration.username = "TEST";
               this.registration.provider.name = "SFR";
         
               this.registration.account.email = "t2u@test.com";
               this.registration.account.password = "aZerty012345";
         */
                /*this.registration.id_provideruseraccount = 2;
                this.registration.provider.name = "FREE";
          
                this.registration.account.email = "guillaume@gaps.com";
                this.registration.account.password = "358jV@WxBS7Jw4Yc5+mtBEv#";
          
                this.oRefreshDataService.getRefreshData(this.registration, this.cntx.token).then((data) => {
                    console.log(data);
                  },
                  (reason) => {
                      console.log(reason.data);
                      return '';
                    }
                  )*/
            };
            /**
             * Fermer la fenêtre d'alerte
             */
            UserProfileController.prototype.closeAlert = function (index) {
                this.alertsUserList.splice(index, 1);
            };
            return UserProfileController;
        }());
        controllers.UserProfileController = UserProfileController;
    })(controllers = myApp.controllers || (myApp.controllers = {}));
})(myApp || (myApp = {}));
// Remember to pass all the services used by the constructor of the Controller.
myApp.registerController('UserProfileController', ['$scope', 'providerUserAccountService', '$route', 'accountLogService', 'refreshDataService', 'chartService']);
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var controllers;
    (function (controllers) {
        /**
         * Class Abstract pour la définition des méthodes et des Attributs générique au Controlleur
         */
        var AGenericController = (function () {
            /*     public get cntx():myApp.bean.Context {
                     return this._cntx;
                 }
                 public set cntx(oCntx: myApp.bean.Context) {
                     this._cntx = oCntx;
                 }*/
            function AGenericController() {
                //console.log("AGenericController constructor");
            }
            return AGenericController;
        }());
        controllers.AGenericController = AGenericController;
    })(controllers = myApp.controllers || (myApp.controllers = {}));
})(myApp || (myApp = {}));
/*myApp.registerController('AController', []);*/ 
/// <reference path='MyController.ts'/>
/// <reference path='DashBoardController.ts'/>
/// <reference path='View2Controller.ts'/>
/// <reference path='LoginController.ts'/>
/// <reference path='SignUpController.ts'/>
/// <reference path='NavBarController.ts'/>
/// <reference path='PricingController.ts'/>
/// <reference path='SupportedProviderController.ts'/>
/// <reference path='UserProfileController.ts'/>
/// <reference path='AGenericController.ts'/> 
/// <reference path="../application.ts" />
/// <reference path='../libs/types/angularjs/angular.d.ts'/>
'use strict';
var myApp;
(function (myApp) {
    var directives;
    (function (directives) {
        var NavBarDirective = (function () {
            function NavBarDirective() {
                this.restrict = 'AC';
                this.template = '<div ng-include="vm.getContentUrl()"></div>';
                this.controller = "myApp.controllers.NavBarController";
                this.link = function (scope, element, attrs) {
                    /*handle all your linking requirements here*/
                    //console.log(scope);
                    /*scope.navBarDirective = this;
                    var oNavBarController = <myApp.controllers.NavBarController>scope.vm;
    
                    this.templateUrl = oNavBarController.getContentUrl();
                    this.compile(element);*/
                };
                ;
            }
            NavBarDirective.prototype.compile = function (templateElement) {
                return {
                    pre: this.link
                };
            };
            NavBarDirective.instance = function () {
                return new NavBarDirective();
            };
            return NavBarDirective;
        }());
        directives.NavBarDirective = NavBarDirective;
    })(directives = myApp.directives || (myApp.directives = {}));
})(myApp || (myApp = {}));
myApp.registerDirective('NavBarDirective', []);
/// <reference path="../application.ts" />
/// <reference path='../libs/types/angularjs/angular.d.ts'/>
'use strict';
/**
 * Directive pour la visualisation des informations sur un Fournisseurs de service.
 */
var myApp;
(function (myApp) {
    var directives;
    (function (directives) {
        var ProviderInformationModalDirective = (function () {
            function ProviderInformationModalDirective() {
                var _this = this;
                this.restrict = 'ACE';
                this.templateUrl = 'views/directives/ProviderInformation.html';
                this.transclude = true;
                this.replace = true;
                this.scope = true;
                this.InitiateDirective();
                this.link = function (scopeDir, element, attrs) {
                    // Injection de la référence du controller dans le $scope
                    _this.currentScope = scopeDir;
                    _this.currentScope.title = attrs.title;
                    _this.myElement = element;
                    var oController;
                    oController = _this.currentScope.vm;
                    if (oController != null) {
                        oController.providerInformationDirective = _this;
                    }
                    // Fermeture de la fenêtre si un click à l'extérieur de celle-ci
                    element.on("click", function (event) {
                        var isClickedElementChildOfPopup = element
                            .find(event.target)
                            .length > 0;
                        if (isClickedElementChildOfPopup)
                            return;
                        _this.hide();
                    });
                    _this.compile(element);
                };
                //console.log(this);
            }
            ProviderInformationModalDirective.prototype.compile = function (templateElement) {
                return {
                    pre: this.link
                };
            };
            Object.defineProperty(ProviderInformationModalDirective.prototype, "isVisible", {
                get: function () {
                    return this._isVisible;
                },
                enumerable: true,
                configurable: true
            });
            ProviderInformationModalDirective.instance = function () {
                return new ProviderInformationModalDirective();
            };
            Object.defineProperty(ProviderInformationModalDirective.prototype, "title", {
                get: function () {
                    return this._title;
                },
                set: function (sTitle) {
                    this._title = sTitle;
                },
                enumerable: true,
                configurable: true
            });
            /**
             * Initialiser les attributs de la directive
             */
            ProviderInformationModalDirective.prototype.InitiateDirective = function () {
                this._isVisible = false;
            };
            /**
             * Afficher la directive
             */
            ProviderInformationModalDirective.prototype.show = function () {
                this.myElement.show();
                this._isVisible = true;
            };
            /**
             * Cacher la directive
             */
            ProviderInformationModalDirective.prototype.hide = function () {
                this.myElement.hide();
                this._isVisible = false;
            };
            /**
             * Valider les modification
             */
            ProviderInformationModalDirective.prototype.submit = function () {
            };
            return ProviderInformationModalDirective;
        }());
        directives.ProviderInformationModalDirective = ProviderInformationModalDirective;
    })(directives = myApp.directives || (myApp.directives = {}));
})(myApp || (myApp = {}));
myApp.registerDirective('ProviderInformationModalDirective', []);
/// <reference path="../application.ts" />
/// <reference path='../libs/types/angularjs/angular.d.ts'/>
'use strict';
/**
 * Directive pour la visualisation des informations sur un Fournisseurs de service.
 */
var myApp;
(function (myApp) {
    var directives;
    (function (directives) {
        var AddProviderAccountModalDirective = (function () {
            function AddProviderAccountModalDirective() {
                var _this = this;
                this.restrict = 'ACE';
                this.templateUrl = 'views/directives/AddProviderAccount.html';
                this.transclude = true;
                this.replace = true;
                this.scope = true;
                this.InitiateDirective();
                this.link = function (scopeDir, element, attrs) {
                    // Injection de la référence du controller dans le $scope
                    _this.currentScope = scopeDir;
                    _this.currentScope.title = attrs.title;
                    _this.myElement = element;
                    var oController;
                    oController = _this.currentScope.vm;
                    if (oController != null) {
                        oController.addProviderAccountDirective = _this;
                    }
                    // Fermeture de la fenêtre si un click à l'extérieur de celle-ci
                    element.on("click", function (event) {
                        var isClickedElementChildOfPopup = element
                            .find(event.target)
                            .length > 0;
                        if (isClickedElementChildOfPopup)
                            return;
                        _this.hide();
                    });
                    _this.compile(element);
                };
                //console.log(this);
            }
            AddProviderAccountModalDirective.prototype.compile = function (templateElement) {
                return {
                    pre: this.link
                };
            };
            Object.defineProperty(AddProviderAccountModalDirective.prototype, "isVisible", {
                get: function () {
                    return this._isVisible;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AddProviderAccountModalDirective.prototype, "title", {
                get: function () {
                    return this._title;
                },
                set: function (sTitle) {
                    this._title = sTitle;
                },
                enumerable: true,
                configurable: true
            });
            AddProviderAccountModalDirective.instance = function () {
                return new AddProviderAccountModalDirective();
            };
            /**
             * Initialiser les attributs de la directive
             */
            AddProviderAccountModalDirective.prototype.InitiateDirective = function () {
                this._isVisible = false;
            };
            /**
             * Afficher la directive
             */
            AddProviderAccountModalDirective.prototype.show = function () {
                this.myElement.show();
                this._isVisible = true;
            };
            /**
             * Cacher la directive
             */
            AddProviderAccountModalDirective.prototype.hide = function () {
                this.myElement.hide();
                this._isVisible = false;
            };
            /**
             * Valider les modification
             */
            AddProviderAccountModalDirective.prototype.submit = function () {
            };
            return AddProviderAccountModalDirective;
        }());
        directives.AddProviderAccountModalDirective = AddProviderAccountModalDirective;
    })(directives = myApp.directives || (myApp.directives = {}));
})(myApp || (myApp = {}));
myApp.registerDirective('AddProviderAccountModalDirective', []);
/// <reference path='NavBarDirective.ts'/>
/// <reference path='ProviderInformationModalDirective.ts'/>
/// <reference path='AddProviderAccountModalDirective.ts'/>
// /// <reference path='View1HttpHandler.ts'/> 
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var bean;
    (function (bean) {
        var Token = (function () {
            function Token(_data, _issuedUtc, _expiresUtc) {
                this._data = _data;
                this._issuedUtc = _issuedUtc;
                this._expiresUtc = _expiresUtc;
                this.data = _data;
                this.issuedUtc = _issuedUtc;
                this.expiresUtc = _expiresUtc;
            }
            return Token;
        }());
        bean.Token = Token;
    })(bean = myApp.bean || (myApp.bean = {}));
})(myApp || (myApp = {}));
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var bean;
    (function (bean) {
        var Context = (function () {
            function Context(newtoken, newisAuth, newemail) {
                this.newtoken = newtoken;
                this.newisAuth = newisAuth;
                this.newemail = newemail;
                this._token = newtoken;
                this._isAuth = newisAuth;
                //this._email = newemail;
                this._account = new bean.Account(newemail, "undefined", -1);
                Context._currentContext = this;
            }
            Object.defineProperty(Context, "currentContext", {
                get: function () {
                    return this._currentContext;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Context.prototype, "token", {
                get: function () {
                    return this._token;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Context.prototype, "isAuth", {
                get: function () {
                    if (this._isAuth != null)
                        return this._isAuth;
                    else
                        return false;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Context.prototype, "account", {
                get: function () {
                    return this._account;
                },
                enumerable: true,
                configurable: true
            });
            /**
             * Effacer le contexte courant
             */
            Context.clear = function () {
                Context._currentContext = null;
            };
            return Context;
        }());
        bean.Context = Context;
    })(bean = myApp.bean || (myApp.bean = {}));
})(myApp || (myApp = {}));
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var bean;
    (function (bean) {
        var Account = (function () {
            function Account(email_, username_, uuid_) {
                this.email_ = email_;
                this.username_ = username_;
                this.uuid_ = uuid_;
                this._email = email_;
                this._username = username_;
                this._uuid = uuid_;
            }
            Object.defineProperty(Account.prototype, "email", {
                get: function () {
                    return this._email;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Account.prototype, "username", {
                get: function () {
                    return this._username;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Account.prototype, "uuid", {
                get: function () {
                    return this._uuid;
                },
                enumerable: true,
                configurable: true
            });
            return Account;
        }());
        bean.Account = Account;
    })(bean = myApp.bean || (myApp.bean = {}));
})(myApp || (myApp = {}));
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var bean;
    (function (bean) {
        var ChartObject = (function () {
            function ChartObject() {
            }
            Object.defineProperty(ChartObject.prototype, "chartConfig", {
                get: function () {
                    return this._chartConfig;
                },
                set: function (value) {
                    this._chartConfig = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartObject.prototype, "chartData", {
                get: function () {
                    return this._chartData;
                },
                set: function (value) {
                    this._chartData = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartObject.prototype, "chartType", {
                get: function () {
                    return this._chartType;
                },
                enumerable: true,
                configurable: true
            });
            /**
             * Retourne le vrai nom du chart dans HighChartJS
             */
            ChartObject.getRealChartName = function (chartType) {
                switch (chartType) {
                    case 'BASIC_LINE':
                        return 'bar';
                    case 'BASIC_PIE':
                        return 'pie';
                    case 'SINGLE_LINE_SERIES':
                        return 'single_line_series';
                    case 'BASIC_AREA':
                        return 'basic_area';
                    default:
                        return "";
                }
            };
            /**
             * Retourne un Objet Graphique
             */
            ChartObject.getNewChart = function (chartType) {
                var oChart;
                oChart = new ChartObject();
                var chartConfig;
                switch (chartType) {
                    case 'bar':
                        chartConfig = {
                            options: {
                                chart: {
                                    type: 'bar'
                                }
                            },
                            series: [],
                            title: {
                                text: 'Title'
                            },
                            loading: false,
                            subtitle: {
                                text: 'subtitle'
                            }
                        };
                        break;
                    case 'pie':
                        chartConfig = {
                            options: {
                                chart: {
                                    type: 'pie'
                                }
                            },
                            series: [{
                                    data: [10, 15, 12, 8, 7]
                                }],
                            title: {
                                text: 'Duree Evenement'
                            },
                            loading: false
                        };
                        break;
                    case 'single_line_series':
                        chartConfig = {
                            options: {
                                chart: {
                                    zoomType: 'x'
                                },
                                series: [{
                                        id: 1,
                                        data: [
                                            [1147651200000, 23.15],
                                            [1147737600000, 23.01],
                                            [1147824000000, 22.73],
                                            [1147910400000, 22.83],
                                            [1147996800000, 22.56],
                                            [1148256000000, 22.88],
                                            [1148342400000, 22.79],
                                            [1148428800000, 23.50],
                                            [1148515200000, 23.74],
                                            [1148601600000, 23.72],
                                            [1148947200000, 23.15],
                                            [1149033600000, 22.65]
                                        ]
                                    }, {
                                        id: 2,
                                        data: [
                                            [1147651200000, 25.15],
                                            [1147737600000, 25.01],
                                            [1147824000000, 25.73],
                                            [1147910400000, 25.83],
                                            [1147996800000, 25.56],
                                            [1148256000000, 25.88],
                                            [1148342400000, 25.79],
                                            [1148428800000, 25.50],
                                            [1148515200000, 26.74],
                                            [1148601600000, 26.72],
                                            [1148947200000, 26.15],
                                            [1149033600000, 26.65]
                                        ]
                                    }],
                                rangeSelector: {
                                    enabled: true
                                },
                                navigator: {
                                    enabled: true
                                }
                            },
                            series: [],
                            title: {
                                text: 'Hello'
                            },
                            useHighStocks: true
                        };
                        break;
                    case 'basic_area':
                        chartConfig = {
                            options: {
                                chart: {
                                    type: 'area'
                                }
                            },
                            title: {
                                text: 'Free Orders'
                            },
                            subtitle: {
                                text: 'Source: <a href="null">' +
                                    'Your Orders Pdf</a>'
                            },
                            xAxis: {
                                allowDecimals: false,
                                labels: {
                                    formatter: function () {
                                        return this.value; // clean, unformatted number for year
                                    },
                                }
                            },
                            yAxis: {
                                title: {
                                    text: 'Price €'
                                },
                                labels: {
                                    formatter: function () {
                                        return this.value + '€';
                                    }
                                }
                            },
                            tooltip: {
                                pointFormat: '{series.name} produced <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
                            },
                            plotOptions: {
                                area: {
                                    pointStart: 1940,
                                    marker: {
                                        enabled: false,
                                        symbol: 'circle',
                                        radius: 2,
                                        states: {
                                            hover: {
                                                enabled: true
                                            }
                                        }
                                    }
                                }
                            },
                            series: [{
                                    name: '2013',
                                    data: [10, 46, 46, 30, 46, 46, 98, 68, 34, 26, 87, 59]
                                }, {
                                    name: '2014',
                                    data: [6, 11, 48, 11, 15, 25, 15, 45, 48, 16, 16, 24]
                                }, {
                                    name: '2015',
                                    data: [5, 25, 50, 12, 15, 20, 42, 66, 86, 16, 16, 71]
                                }]
                        };
                        break;
                    default:
                        alert(chartType + " tpye are not supported");
                }
                oChart._chartConfig = chartConfig;
                var oTheme = new bean.ChartDarkTheme();
                Highcharts.setOptions(oTheme.theme);
                oChart._chartType = chartType;
                return oChart;
            };
            return ChartObject;
        }());
        bean.ChartObject = ChartObject;
    })(bean = myApp.bean || (myApp.bean = {}));
})(myApp || (myApp = {}));
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var bean;
    (function (bean) {
        var ChartTheme = (function () {
            function ChartTheme() {
            }
            Object.defineProperty(ChartTheme.prototype, "theme", {
                get: function () {
                    return this._theme;
                },
                enumerable: true,
                configurable: true
            });
            return ChartTheme;
        }());
        bean.ChartTheme = ChartTheme;
    })(bean = myApp.bean || (myApp.bean = {}));
})(myApp || (myApp = {}));
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var bean;
    (function (bean) {
        var ChartDarkTheme = (function (_super) {
            __extends(ChartDarkTheme, _super);
            function ChartDarkTheme() {
                _super.call(this);
                this._theme = {
                    colors: ["#2b908f", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
                        "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
                    chart: {
                        backgroundColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
                            stops: [
                                [0, '#2a2a2b'],
                                [1, '#3e3e40']
                            ]
                        },
                        style: {
                            fontFamily: "'Unica One', sans-serif"
                        },
                        plotBorderColor: '#606063'
                    },
                    title: {
                        style: {
                            color: '#E0E0E3',
                            textTransform: 'uppercase',
                            fontSize: '20px'
                        }
                    },
                    subtitle: {
                        style: {
                            color: '#E0E0E3',
                            textTransform: 'uppercase'
                        }
                    },
                    xAxis: {
                        gridLineColor: '#707073',
                        labels: {
                            style: {
                                color: '#E0E0E3'
                            }
                        },
                        lineColor: '#707073',
                        minorGridLineColor: '#505053',
                        tickColor: '#707073',
                        title: {
                            style: {
                                color: '#A0A0A3'
                            }
                        }
                    },
                    yAxis: {
                        gridLineColor: '#707073',
                        labels: {
                            style: {
                                color: '#E0E0E3'
                            }
                        },
                        lineColor: '#707073',
                        minorGridLineColor: '#505053',
                        tickColor: '#707073',
                        tickWidth: 1,
                        title: {
                            style: {
                                color: '#A0A0A3'
                            }
                        }
                    },
                    tooltip: {
                        backgroundColor: 'rgba(0, 0, 0, 0.85)',
                        style: {
                            color: '#F0F0F0'
                        }
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                color: '#B0B0B3'
                            },
                            marker: {
                                lineColor: '#333'
                            }
                        },
                        boxplot: {
                            fillColor: '#505053'
                        },
                        candlestick: {
                            lineColor: 'white'
                        },
                        errorbar: {
                            color: 'white'
                        }
                    },
                    legend: {
                        itemStyle: {
                            color: '#E0E0E3'
                        },
                        itemHoverStyle: {
                            color: '#FFF'
                        },
                        itemHiddenStyle: {
                            color: '#606063'
                        }
                    },
                    credits: {
                        style: {
                            color: '#666'
                        }
                    },
                    labels: {
                        style: {
                            color: '#707073'
                        }
                    },
                    drilldown: {
                        activeAxisLabelStyle: {
                            color: '#F0F0F3'
                        },
                        activeDataLabelStyle: {
                            color: '#F0F0F3'
                        }
                    },
                    navigation: {
                        buttonOptions: {
                            symbolStroke: '#DDDDDD',
                            theme: {
                                fill: '#505053'
                            }
                        }
                    },
                    // scroll charts
                    rangeSelector: {
                        buttonTheme: {
                            fill: '#505053',
                            stroke: '#000000',
                            style: {
                                color: '#CCC'
                            },
                            states: {
                                hover: {
                                    fill: '#707073',
                                    stroke: '#000000',
                                    style: {
                                        color: 'white'
                                    }
                                },
                                select: {
                                    fill: '#000003',
                                    stroke: '#000000',
                                    style: {
                                        color: 'white'
                                    }
                                }
                            }
                        },
                        inputBoxBorderColor: '#505053',
                        inputStyle: {
                            backgroundColor: '#333',
                            color: 'silver'
                        },
                        labelStyle: {
                            color: 'silver'
                        }
                    },
                    navigator: {
                        handles: {
                            backgroundColor: '#666',
                            borderColor: '#AAA'
                        },
                        outlineColor: '#CCC',
                        maskFill: 'rgba(255,255,255,0.1)',
                        series: {
                            color: '#7798BF',
                            lineColor: '#A6C7ED'
                        },
                        xAxis: {
                            gridLineColor: '#505053'
                        }
                    },
                    scrollbar: {
                        barBackgroundColor: '#808083',
                        barBorderColor: '#808083',
                        buttonArrowColor: '#CCC',
                        buttonBackgroundColor: '#606063',
                        buttonBorderColor: '#606063',
                        rifleColor: '#FFF',
                        trackBackgroundColor: '#404043',
                        trackBorderColor: '#404043'
                    },
                    // special colors for some of the
                    legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
                    background2: '#505053',
                    dataLabelsColor: '#B0B0B3',
                    textColor: '#C0C0C0',
                    contrastTextColor: '#F0F0F3',
                    maskColor: 'rgba(255,255,255,0.3)'
                };
            }
            return ChartDarkTheme;
        }(bean.ChartTheme));
        bean.ChartDarkTheme = ChartDarkTheme;
    })(bean = myApp.bean || (myApp.bean = {}));
})(myApp || (myApp = {}));
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var bean;
    (function (bean) {
        var Alert = (function () {
            function Alert(type_, msg_) {
                this.type_ = type_;
                this.msg_ = msg_;
                this._type = type_;
                this._msg = msg_;
            }
            Object.defineProperty(Alert.prototype, "type", {
                get: function () {
                    return this._type;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Alert.prototype, "msg", {
                get: function () {
                    return this._msg;
                },
                enumerable: true,
                configurable: true
            });
            return Alert;
        }());
        bean.Alert = Alert;
    })(bean = myApp.bean || (myApp.bean = {}));
})(myApp || (myApp = {}));
/// <reference path ='Token.ts'/>
/// <reference path ='Context.ts'/>
/// <reference path ='Account.ts'/>
/// <reference path ='ChartObject.ts'/>
/// <reference path ='ChartTheme.ts'/>
/// <reference path ='ChartDarkTheme.ts'/>
/// <reference path ='Alert.ts'/> 
/// <reference path="../application.ts" />
'use strict';
var myApp;
(function (myApp) {
    var util;
    (function (util) {
        //
        // MS documentation : https://msdn.microsoft.com/fr-fr/library/dn265046(v=vs.85).aspx
        //
        var WebCrypto = (function () {
            function WebCrypto($q) {
                this.$q = $q;
                //this._localStorageService = localStorageService;
                this.crypto = window.crypto; // || window.msCrypto; // pour IE 11
                //this.keyGeneration();
                this.localIQService = $q;
            }
            /**
             * RSASSA-PKCS1-v1_5 - generateKey (https://github.com/diafygi/webcrypto-examples)
             */
            WebCrypto.prototype.keyGeneration = function () {
                var _this = this;
                var deferred = this.localIQService.defer();
                this.crypto.subtle.generateKey({
                    name: "RSA-OAEP",
                    modulusLength: 4096,
                    publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
                    hash: {
                        name: "SHA-512"
                    },
                }, false, //whether the key is extractable (i.e. can be used in exportKey)
                ["encrypt", "decrypt"] //must contain both "encrypt" and "decrypt"
                )
                    .then(function (key) {
                    //returns a keypair object
                    /*console.log(key);
                    console.log(key.publicKey);
                    console.log(key.privateKey);*/
                    _this.currentKeyPair = key;
                    deferred.resolve(key);
                }, 
                // Error Should also return a string to be compatible with the return type
                function (err) {
                    deferred.reject(err);
                });
                return deferred.promise;
            };
            /**
             *RSASSA-PKCS1-v1_5 - importKey
             */
            /*  public importKey(): void{
                  this.crypto.subtle.importKey(
                      "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
                      {   //this is an example jwk key, other key types are Uint8Array objects
                          kty: "RSA",
                          e: "AQAB",
                          n: "vGO3eU16ag9zRkJ4AK8ZUZrjbtp5xWK0LyFMNT8933evJoHeczexMUzSiXaLrEFSyQZortk81zJH3y41MBO_UFDO_X0crAquNrkjZDrf9Scc5-MdxlWU2Jl7Gc4Z18AC9aNibWVmXhgvHYkEoFdLCFG-2Sq-qIyW4KFkjan05IE",
                          alg: "RS256",
                          ext: true,
                      },
                      {   //these are the algorithm options
                          name: "RSASSA-PKCS1-v1_5",
                          hash: {name: "SHA-512"}, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
                      },
                      false, //whether the key is extractable (i.e. can be used in exportKey)
                      ["verify"] //"verify" for public key import, "sign" for private key imports
                  )
                  .then(function(publicKey){
                      //returns a publicKey (or privateKey if you are importing a private key)
                      console.log(publicKey);
                  })
                  .catch(function(err){
                      console.error(err);
                  });
              }*/
            WebCrypto.prototype.exportPublicKey = function () {
                this.crypto.subtle.exportKey("jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
                this.currentKeyPair.publicKey //can be a publicKey or privateKey, as long as extractable was true
                )
                    .then(function (keydata) {
                    //returns the exported key data
                    console.log(keydata);
                })
                    .catch(function (err) {
                    console.error(err);
                });
            };
            WebCrypto.prototype.exportPrivateKey = function () {
                this.crypto.subtle.exportKey("jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
                this.currentKeyPair.privateKey //can be a publicKey or privateKey, as long as extractable was true
                )
                    .then(function (keydata) {
                    //returns the exported key data
                    console.log(keydata);
                })
                    .catch(function (err) {
                    console.error(err);
                });
            };
            WebCrypto.prototype.signMessage = function (data) {
                this.crypto.subtle.sign({
                    name: "RSASSA-PKCS1-v1_5",
                }, this.currentKeyPair.privateKey, //from generateKey or importKey above
                data //ArrayBuffer of data you want to sign
                )
                    .then(function (signature) {
                    //returns an ArrayBuffer containing the signature
                    console.log(new Uint8Array(signature));
                })
                    .catch(function (err) {
                    console.error(err);
                });
            };
            WebCrypto.prototype.verifyMessage = function (data, publicKey, signature) {
                this.crypto.subtle.verify({
                    name: "RSASSA-PKCS1-v1_5",
                }, publicKey, //from generateKey or importKey above
                signature, //ArrayBuffer of the signature
                data //ArrayBuffer of the data
                ).then(function (isvalid) {
                    //returns a boolean on whether the signature is true or not
                    console.log(isvalid);
                }, function (err) {
                    console.error(err);
                });
            };
            WebCrypto.prototype.encryptMessage = function (data, publicKey) {
                var deferred = this.localIQService.defer();
                //var dataBuffer = new ArrayBuffer(data);
                this.crypto.subtle.encrypt({
                    name: "RSA-OAEP",
                }, publicKey, //from generateKey or importKey above
                this.str2ab(data) //ArrayBuffer of data you want to encrypt
                ).then(function (encrypted) {
                    //returns an ArrayBuffer containing the encrypted data
                    //console.log(new Uint8Array(encrypted));
                    //console.log(encrypted);
                    //console.log(this.ab2str(encrypted));
                    deferred.resolve(encrypted);
                }, function (err) {
                    //console.error(err);
                    deferred.reject(err);
                });
                return deferred.promise;
            };
            WebCrypto.prototype.decryptMessage = function (data) {
                var _this = this;
                this.crypto.subtle.decrypt({
                    name: "RSA-OAEP",
                }, this.currentKeyPair.privateKey, //from generateKey or importKey above
                data //ArrayBuffer of the data
                ).then(function (decrypted) {
                    //returns an ArrayBuffer containing the decrypted data
                    console.log(new Uint8Array(decrypted));
                    console.log(_this.ab2str(decrypted));
                    console.log(decrypted);
                }, function (err) {
                    //console.error(err);
                    console.error(err);
                });
            };
            WebCrypto.prototype.ab2str = function (buf) {
                return String.fromCharCode.apply(null, new Uint16Array(buf));
            };
            WebCrypto.prototype.str2ab = function (str) {
                var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
                var bufView = new Uint16Array(buf);
                for (var i = 0, strLen = str.length; i < strLen; i++) {
                    bufView[i] = str.charCodeAt(i);
                }
                return buf;
            };
            return WebCrypto;
        }());
        util.WebCrypto = WebCrypto;
    })(util = myApp.util || (myApp.util = {}));
})(myApp || (myApp = {}));
/// <reference path='WebCrypto.ts'/>
/// <reference path='Configuration.ts'/>
// /// <reference path='AesCBC.ts'/> 
/**
 *
 */
// Unstable branch typescript 1.5 alpha <https://github.com/borisyankov/DefinitelyTyped/tree/tsc-1.5.0-alpha>
/// <reference path='libs/types/jquery/jquery.d.ts'/>
/// <reference path='libs/types/angularjs/angular.d.ts'/>
/// <reference path='libs/types/angular-local-storage/angular-local-storage.d.ts'/>
/// <reference path='libs/types/angularjs/angular-resource.d.ts'/>
/// <reference path='libs/types/angularjs/angular-route.d.ts'/>
/// <reference path='libs/types/angularjs/angular-cookies.d.ts'/>
/// <reference path='libs/types/highcharts/highcharts.d.ts'/>
/// <reference path='application.ts'/>
/// <reference path='filters/_filters.ts'/>
/// <reference path='services/_services.ts'/>
/// <reference path='controllers/_controllers.ts'/>
/// <reference path='directives/_directives.ts'/>
/// <reference path='handlers/_handlers.ts'/>
/// <reference path='bean/_bean.ts'/>
/// <reference path='util/_util.ts'/> 
//# sourceMappingURL=out.js.map