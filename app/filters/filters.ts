/// <reference path="../application.ts" />
'use strict';

module myApp.filters {

    export class RangeTo implements IFilter {
        filter (start: number, end: number) {
            var out = [];
            for (var i = start; i < end; ++i) out.push(i)
            return out
        }
    }

    export class Splice implements IFilter {
        filter (input: Array<String>, start: number, howMany: number) {
            return input.splice(start, howMany)
        }
    }

}

myApp.registerFilter('RangeTo', []);
myApp.registerFilter('Splice', []);
