/// <reference path="../application.ts" />

'use strict';

module myApp.controllers {
	export interface IUserProfileControllerScope extends IControllerScope {
      message: any;
	}

  export class UserProfileController implements IController {

    // Le context du controlleur
    protected _cntx: myApp.bean.Context;
    public get cntx():myApp.bean.Context {
        return this._cntx;
    }

    private scope: any;

    private oProviderUserAccountService: myApp.services.ProviderUserAccountService;
    /*private oSupportedProviderController: myApp.controllers.SupportedProviderController;*/
    private oAccountLogService: myApp.services.AccountLogService;
    public myProviderList: any;
    public myAccountLogList: any;

    private route: ng.route.IRouteService;

    private myAccountsLog = {
          account: { email: "", username: "" }
    };

    /* Clear Data when is possible */
    private mainPassword: any;

    private oRefreshDataService: myApp.services.RefreshDataService;
    private registration = {
          id_provideruseraccount : 0,
          username: "",
          email: "",
          password: "",
          confirmPassword: "",
          account: { email: "", password: "" },
          provider: { name: "" }
    };    

    private oChartService: myApp.services.ChartService;
    private chartResult: any;   
    private chart = {
          account: { email: '' }
    };  

    // Alert Box
    //private alertsUser: myApp.bean.Alert;
    private alertsUserList: Array<myApp.bean.Alert>;

    constructor (private $scope: IUserProfileControllerScope,
                 private _oProviderUserAccountService: myApp.services.ProviderUserAccountService,
                 private $route: ng.route.IRouteService,
                 private _oAccountLogService: myApp.services.AccountLogService,
                 private _RefreshDataService: myApp.services.RefreshDataService,
                 private _oChartService:  myApp.services.ChartService) {

      // Injection de la référence du controller dans le $scope
  		$scope.vm = this;

      this.scope = $scope;
      this.route = $route;

      // Récupération du context courrant
      this._cntx = myApp.bean.Context.currentContext;      

      this.oProviderUserAccountService = _oProviderUserAccountService;
      this.oAccountLogService = _oAccountLogService;
      this.oChartService =_oChartService;

      this.alertsUserList = new Array();

        // Récupération de MES logs
        if (this.myAccountLogList == null) {
          this.myAccountsLog.account.email = this.cntx.account.email;
          this.getMyAccountLog(this.myAccountsLog);
        }

      // Chargement des Graphiques
      this.loadingAllCharts();
    }

  /**
   * Chargement de tous les Charts du Client
   */
    public loadingAllCharts(){
      var oChart: myApp.bean.ChartObject;

      if (this.oChartService != null) {
       
        // Récupération des charts du Client connecté
        /*this.chart.account.email = this.cntx.account.email;*/

        this.oChartService.getChart(this.chart, this.cntx.token).then(
          (data) => {
            console.log(data); 

            this.chartResult = data;
          },
          (reason) => {
            var parser = new DOMParser();
            var doc = parser.parseFromString(reason.data, 'text/html');
            doc.firstChild; //this is what you're after.

            var nList: NodeList = doc.getElementsByTagName("p");

            var serverMessage: string;

            for (var n = 0; n <= nList.length; n++)
            {         
                var nNode: Node = nList.item(n);

                serverMessage = nNode.textContent;
                break;
            }

            this.alertsUserList.push(new myApp.bean.Alert('danger', reason.status + " - [" + reason.statusText + "] " + serverMessage));
            
            return ''; 
          }
        )
      }
    }    

    /**
     * Récupération de MES comptes de fournisseurs en base de données
     */
    public getMyAccountLog(parameters: any): void {
       if (this.oAccountLogService != null) {
        this.oAccountLogService.getAccountLog(parameters, this.cntx.token).then((data) => {
                  this.myAccountLogList = data;
                  //console.log(data);
        },
        // Error Should also return a string to be compatible with the return type
        (reason) => {
           var parser = new DOMParser();
            var doc = parser.parseFromString(reason.data, 'text/html');
            doc.firstChild; //this is what you're after.

            var nList: NodeList = doc.getElementsByTagName("p");

            var serverMessage: string;

            for (var n = 0; n <= nList.length; n++)
            {         
                var nNode: Node = nList.item(n);

                serverMessage = nNode.textContent;
                break;
            }

            this.alertsUserList.push(new myApp.bean.Alert('danger', reason.status + " - [" + reason.statusText + "] " + serverMessage));

          return '';}
        )
      }
    }

  /**
   *
   */
    public getRefreshData(): void {
       // console.log(this.mainPassword);

       this.mainPassword = null;  

       /*
      this.registration.username = "TEST";
      this.registration.provider.name = "SFR";

      this.registration.account.email = "t2u@test.com";
      this.registration.account.password = "aZerty012345";
*/
      /*this.registration.id_provideruseraccount = 2;
      this.registration.provider.name = "FREE";

      this.registration.account.email = "guillaume@gaps.com";
      this.registration.account.password = "358jV@WxBS7Jw4Yc5+mtBEv#";

      this.oRefreshDataService.getRefreshData(this.registration, this.cntx.token).then((data) => {
          console.log(data);
        },
        (reason) => {     
            console.log(reason.data);
            return '';
          }
        )*/
    }

    /**
     * Fermer la fenêtre d'alerte
     */
    public closeAlert(index: number): void {
       this.alertsUserList.splice(index, 1);
    }         
  }
}

// Remember to pass all the services used by the constructor of the Controller.
myApp.registerController('UserProfileController', ['$scope', 'providerUserAccountService' , '$route', 'accountLogService', 'refreshDataService', 'chartService']);
