/// <reference path="../application.ts" />

'use strict';

module myApp.services {

    export class MyService implements IService {
        private meaningOfLife = 42;
        constructor () {

        }
        someMethod () {
            return 'Meaning of life is ' + this.meaningOfLife;
        }
    }

}

myApp.registerService('MyService', []);
