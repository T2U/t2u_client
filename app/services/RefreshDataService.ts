/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />

'use strict';

module myApp.services {

    export class RefreshDataService extends HttpHandlerService {

        private cntx: myApp.bean.Context;

        constructor( $http: ng.IHttpService )
        {
            super( $http );

            this.handlerUrl = "/refreshdata";

            this.cntx = myApp.bean.Context.currentContext;
        }

        /**
         * Retourner la liste des données rafraichie pour le ProviderUserAccount choisie. 
         */
        public getRefreshData(data: any, token: any): ng.IPromise< any >
        {
            if (token != null) {
                var header = {
                    "headers": {
                        "Token": angular.toJson(token)
                    }
                };
            }
            this.handlerUrlParam = "?criteria=" + encodeURIComponent(angular.toJson(data));

            console.log(this.handlerUrl);
            
            return this.useGetHandler(header);
        }        
    }
}

myApp.registerService('RefreshDataService', ['$http']);
