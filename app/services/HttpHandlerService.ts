/// <reference path="../application.ts" />

'use strict';

// SRC <https://kodeyak.wordpress.com/2014/11/26/angularjs-in-typescript-services-and-http/>
// SRC <http://stackoverflow.com/questions/12827266/get-and-set-in-typescript>
module myApp.services {

  export class HttpHandlerService implements IService
  {
    /* Référence vers l'objet HTTP */
    private _httpService: ng.IHttpService;
    get httpService():ng.IHttpService {
        return this._httpService;
    }
    set httpService(thehttpService:ng.IHttpService) {
        this._httpService = thehttpService;
    }

    protected _baseUrl: String = "";
    protected _handlerUrl: String = "";
    public get handlerUrl(): String {
      this.updateBaseURL();

      var tempValue: String;

      if(this._handlerUrlParam == null)
        tempValue = ( this._baseUrl.toString() + this._handlerUrl.toString() );
      else
        tempValue = ( this._baseUrl.toString() + this._handlerUrl.toString() + this._handlerUrlParam.toString() );

      return (tempValue);
    }
    public set handlerUrl(value:String) {
      this._handlerUrl = value;
    }

    protected _handlerUrlParam: String = "";
    public get handlerUrlParam(): String {
      return (this._handlerUrlParam);
    }
    public set handlerUrlParam(value:String) {
      this._handlerUrlParam = value;
    }    

    constructor( $http: ng.IHttpService )
    {
      this.httpService = $http;

      if(myApp.services.Configuration.instance == null)
        var configurationObject = new myApp.services.Configuration(this.httpService, "config/config.json");

      this.updateBaseURL();
      /*if(myApp.services.Configuration.instance.SSL_enabled)
        this._baseUrl = "https://";
      else
        this._baseUrl = "http://";

      this._baseUrl = this._baseUrl + myApp.services.Configuration.instance.HostName.toString() +
       ":" + myApp.services.Configuration.instance.Port.toString() + 
       "/" + myApp.services.Configuration.instance.WebServiceName.toString();*/

       /*console.log("HttpHandlerService:_baseUrl:" + this._baseUrl);*/
    }

    updateBaseURL(): void
    {
      if(myApp.services.Configuration.instance.SSL_enabled)
        this._baseUrl = "https://";
      else
        this._baseUrl = "http://";

      this._baseUrl = this._baseUrl + myApp.services.Configuration.instance.HostName.toString() +
       ":" + myApp.services.Configuration.instance.Port.toString() + 
       myApp.services.Configuration.instance.WebServiceName.toString();
    }

    useGetHandler( params: any ): ng.IPromise< any >
    {
      var result: ng.IPromise< any > = this._httpService.get( this.handlerUrl.toString(), params )
      .then( ( response: any ): ng.IPromise< any > => this.handlerResponded( response, params ) );
      return result;
    }

    usePostHandler( params: any ): ng.IPromise< any >
    {
      var result: ng.IPromise< any >  = this._httpService.post( this.handlerUrl.toString(), params )
      .then( ( response: any ): ng.IPromise< any > => this.handlerResponded( response, params ) );
      return result;
    }

    useDeleteHandler( params: any ): ng.IPromise< any >
    {
    console.log(this.handlerUrl);
    
      var result: ng.IPromise< any >  = this._httpService.delete( this.handlerUrl.toString(), params )
      .then( ( response: any ): ng.IPromise< any > => this.handlerResponded( response, params ) );
      return result;
    }    

    handlerResponded( response: any, params: any ): any
    {
      response.data.requestParams = params;
      return response.data;
    }

  } // HttpHandlerService class
}


myApp.registerService('HttpHandlerService', ['$http']);