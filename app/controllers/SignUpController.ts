/// <reference path="../application.ts" />
/// <reference path="../services/AuthenticationService.ts" />

'use strict';

module myApp.controllers {

	export interface ISignUpControllerScope extends IControllerScope {
    	registration: any;
  	}

	export class SignUpController implements IController {

		//	Custom Services
		private oAuthenticationService: myApp.services.AuthenticationService;

    private scope: ISignUpControllerScope;

    private location: ng.ILocationService;

    private timeoutLoginRedirect: ng.ITimeoutService;

		private registration = {
          username: "",
        	email: "",
        	password: "",
        	confirmPassword: ""
    };

    /* TODO : Cet Objet ne sert qu'à mapper les noms avec les nom des beans sur le serveur userName -> EMAIL*/
    private registrationBinding = {
          username: "",
          email: "",
          password: ""
    };

    // Alert Box
    private alertsUser: myApp.bean.Alert;

    constructor ( private $scope: ISignUpControllerScope,
                    private $location: ng.ILocationService,
                    private $timeout: ng.ITimeoutService,
                    private oService: myApp.services.AuthenticationService) {

      // Injection de la référence du controller dans le $scope
      $scope.vm = this;

		  //  Récupération du service
	    this.oAuthenticationService = oService;

  	  this.scope = $scope;
  	  this.location = $location;
  	  this.timeoutLoginRedirect = $timeout;
    }

    /**
     * Enregistrement d'un nouvel utilisateur depuis l'objet registration
     **/
    public signUp(formIsValid: boolean) : void {

      if(formIsValid)
      {
        this.registrationBinding.username  = this.registration.username;
        this.registrationBinding.email = this.registration.email;
        this.registrationBinding.password = this.registration.password;

        //console.log(this.registrationBinding);

   			this.oAuthenticationService._saveRegistration(this.registrationBinding).then( ( data ) => {
              this.closeAlert();
              this.alertsUser = new myApp.bean.Alert('success', "User has been registered successfully, you will be redicted to login page in 2 seconds.");

              this.startTimer();
        		},
              // Error Should also return a string to be compatible with the return type
              (reason) => {
                  this.closeAlert();
                  
                  var parser = new DOMParser();
                  var doc = parser.parseFromString(reason.data, 'text/html');
                  doc.firstChild; //this is what you're after.

                  var nList: NodeList = doc.getElementsByTagName("p");

                  var serverMessage: string;

                  for (var n = 0; n <= nList.length; n++)
                  {         
                      var nNode: Node = nList.item(n);

                      serverMessage = nNode.textContent;
                      break;
                  }

                  this.alertsUser = new myApp.bean.Alert('danger', reason.status + " - [" + reason.statusText + "] " + serverMessage);

                  return '';
              }
          )
      }
		}

    /**
     * Redirection automatique vers la page de connection
     **/
		private startTimer(): void {
      var timer = this.timeoutLoginRedirect(()=> {
                    this.timeoutLoginRedirect.cancel(timer);
                    this.location.path('/login');
                  }, 2200);
    }

    /**
     * Fermer la fenêtre d'alerte
     */
    public closeAlert(): void{
       this.alertsUser = null;
    }    
  }
}

// Remember to pass all the services used by the constructor of the Controller.
// WARNING AuthenticationService but authenticationService doesn't work
myApp.registerController('SignUpController', ['$scope', '$location', '$timeout', 'authenticationService']);