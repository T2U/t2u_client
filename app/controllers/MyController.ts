/// <reference path="../application.ts" />

'use strict';

module myApp.controllers {
	export interface IMyControllerScope extends IControllerScope {
      message: any;
	}

  export class MyController implements IController {

    // Le context du controlleur
    protected _cntx: myApp.bean.Context;
    public get cntx():myApp.bean.Context {
        return this._cntx;
    }

    private oSubmitRequestService: myApp.services.SubmitRequestService;  

    constructor (private $scope: IMyControllerScope, private myService: myApp.services.MyService,
      private _SubmitRequestService: myApp.services.SubmitRequestService) {
      // Injection de la référence du controller dans le $scope
  		$scope.vm = this;

      // Récupération du context courrant	
      this._cntx = myApp.bean.Context.currentContext;

      this.oSubmitRequestService = _SubmitRequestService;
    }
  }
}

// Remember to pass all the services used by the constructor of the Controller.
myApp.registerController('MyController', ['$scope', 'myService', 'submitRequestService']);
