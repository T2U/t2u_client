/// <reference path="../application.ts" />

'use strict';

module myApp.bean {

    export class Account {

        private _email: string;
          get email(): string {
            return this._email;
        }

        private _username: string;
          get username(): string {
            return this._username;
        }

        private _uuid: number;
          get uuid(): number {
            return this._uuid;
        }

        constructor(private email_: string, private username_: string, private uuid_: number  )
        {
            this._email = email_;
            this._username = username_;
            this._uuid = uuid_;
        }
    }
}