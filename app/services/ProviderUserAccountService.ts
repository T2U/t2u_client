/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />

'use strict';

/**
 * Service pour la récupération des informations sur les fournisseurs de service
 */
module myApp.services {

    export class ProviderUserAccountService extends HttpHandlerService {

        private cntx: myApp.bean.Context;

        constructor( $http: ng.IHttpService )
        {
            super( $http );
            
            this.handlerUrl = "/provideruseraccount";

            this.cntx = myApp.bean.Context.currentContext;
        }

        /**
         * Retourner une liste d'information sur la base de donnée. 
         */
        public getProviderUserAccount(data: any, token: any): ng.IPromise< any >
        {
            if (token != null) {
                var header = {
                    "headers": {
                        "Token": angular.toJson(token)
                    }
                };
            }
            this.handlerUrlParam = "?criteria=" + encodeURIComponent(angular.toJson(data));
              //+ ",token=" + angular.toJson(token);
            //console.log(this.handlerUrl);
            return this.useGetHandler(header);
        }        

        /**
         * Ajouter un nouveau compte de frounisseur pour un utilisateur
         */
        public postProviderUserAccount(data: any,token: any): ng.IPromise< any >
        {
            if (token != null) {
                var header = {
                    "headers": {
                        "Token": angular.toJson(token)
                    }
                };
            }

            return this.usePostHandler( data );
        }  

        /**
         * Supprimer un compte de frounisseur pour un utilisateur
         */
        public deleteProviderUserAccount(data: any,token: any): ng.IPromise< any >
        {
            if (token != null) {
                var header = {
                    "headers": {
                        "Token": angular.toJson(token)
                    }
                };
            }
            
            this.handlerUrlParam = "?criteria=" + encodeURIComponent(angular.toJson(data));
            
            return this.useDeleteHandler( header );
        }              
    }
}

myApp.registerService('ProviderUserAccountService', ['$http']);
