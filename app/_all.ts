/**
 * 
 */

// Unstable branch typescript 1.5 alpha <https://github.com/borisyankov/DefinitelyTyped/tree/tsc-1.5.0-alpha>
/// <reference path='libs/types/jquery/jquery.d.ts'/>
/// <reference path='libs/types/angularjs/angular.d.ts'/>
/// <reference path='libs/types/angular-local-storage/angular-local-storage.d.ts'/>
/// <reference path='libs/types/angularjs/angular-resource.d.ts'/>
/// <reference path='libs/types/angularjs/angular-route.d.ts'/>
/// <reference path='libs/types/angularjs/angular-cookies.d.ts'/>
/// <reference path='libs/types/highcharts/highcharts.d.ts'/>

/// <reference path='application.ts'/>

/// <reference path='filters/_filters.ts'/>
/// <reference path='services/_services.ts'/>
/// <reference path='controllers/_controllers.ts'/>
/// <reference path='directives/_directives.ts'/>
/// <reference path='handlers/_handlers.ts'/>

/// <reference path='bean/_bean.ts'/>
/// <reference path='util/_util.ts'/>