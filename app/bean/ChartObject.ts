/// <reference path="../application.ts" />

'use strict';

module myApp.bean {

    export class ChartObject {

        private _chartConfig: any;
        get chartConfig():any {
            return this._chartConfig;
        }
        set chartConfig(value:any) {
            this._chartConfig = value;
        }    

        private _chartData: any;
        get chartData():any {
            return this._chartData;
        }
        set chartData(value:any) {
            this._chartData = value;
        }    

        private _chartType: String;
        get chartType():String {
            return this._chartType;
        }

        constructor()
        {
        }

        /**
         * Retourne le vrai nom du chart dans HighChartJS
         */
        public static getRealChartName(chartType: string): string {
            switch (chartType) 
            { 
                case 'BASIC_LINE':
                    return 'bar';
                case 'BASIC_PIE':
                    return 'pie';
                case 'SINGLE_LINE_SERIES':
                    return 'single_line_series';
                case 'BASIC_AREA':
                    return 'basic_area';    
                default:
                    return "";
            }
        }

        /**
         * Retourne un Objet Graphique
         */
        public static getNewChart(chartType: string): ChartObject
        {
            var oChart: ChartObject;
            oChart = new ChartObject();
            var chartConfig: any;

            switch (chartType) 
            { 
                case'bar': 
                    chartConfig = {
                      options: {
                        chart: {
                          type: 'bar'
                        }
                      },
                      series: [],
                      title: {
                        text: 'Title'
                      },
                      loading: false,
                      subtitle: {
                        text: 'subtitle'
                      }
                    };
                  break; 
                case 'pie':
                 chartConfig = {
                  options: {
                    chart: {
                      type: 'pie'
                    }
                    },
                    series: [{
                      data: [10, 15, 12, 8, 7]
                      }],
                      title: {
                        text: 'Duree Evenement'
                        },
                        loading: false
                      }; 
                    break;
                case 'single_line_series':
                    chartConfig = {
                        options: {
                            chart: {
                                zoomType: 'x'
                            }, 
                            series: [{
                        id: 1,
                        data: [
                            [1147651200000, 23.15],
                            [1147737600000, 23.01],
                            [1147824000000, 22.73],
                            [1147910400000, 22.83],
                            [1147996800000, 22.56],
                            [1148256000000, 22.88],
                            [1148342400000, 22.79],
                            [1148428800000, 23.50],
                            [1148515200000, 23.74],
                            [1148601600000, 23.72],
                            [1148947200000, 23.15],
                            [1149033600000, 22.65]
                        ]
                        },   {
                            id: 2,
                            data: [
                                [1147651200000, 25.15],
                                [1147737600000, 25.01],
                                [1147824000000, 25.73],
                                [1147910400000, 25.83],
                                [1147996800000, 25.56],
                                [1148256000000, 25.88],
                                [1148342400000, 25.79],
                                [1148428800000, 25.50],
                                [1148515200000, 26.74],
                                [1148601600000, 26.72],
                                [1148947200000, 26.15],
                                [1149033600000, 26.65]
                            ]
                        }],
                        rangeSelector: {
                            enabled: true
                        },
                        navigator: {
                            enabled: true
                        }
                        },
                        series: [],
                        title: {
                            text: 'Hello'
                        },
                        useHighStocks: true
                    }
                    break;
                case 'basic_area':
                    chartConfig = {
                     options: {
                        chart: {
                            type: 'area'
                            }
                        },
                        title: {
                            text: 'Free Orders'
                        },
                        subtitle: {
                            text: 'Source: <a href="null">' +
                                'Your Orders Pdf</a>'
                        },
                        xAxis: {
                            allowDecimals: false,
                            labels: {
                                formatter: function () {
                                    return this.value; // clean, unformatted number for year
                                },
                                //data :[1,2,3,4,5,6,7,8,9,10,11,12]
                            }
                        },
                        yAxis: {
                            title: {
                                text: 'Price €'
                            },
                            labels: {
                                formatter: function () {
                                    return this.value + '€';
                                }
                            }
                        },
                        tooltip: {
                            pointFormat: '{series.name} produced <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
                        },
                        plotOptions: {
                            area: {
                                pointStart: 1940,
                                marker: {
                                    enabled: false,
                                    symbol: 'circle',
                                    radius: 2,
                                    states: {
                                        hover: {
                                            enabled: true
                                        }
                                    }
                                }
                            }
                        },
                        series: [{
                            name: '2013',
                            data: [10, 46, 46, 30, 46, 46, 98, 68, 34, 26, 87, 59]
                        }, {
                            name: '2014',
                            data: [6, 11, 48, 11, 15, 25, 15, 45, 48, 16, 16, 24]
                        },{
                            name: '2015',
                            data: [5, 25, 50, 12, 15, 20, 42, 66, 86, 16, 16, 71]
                        }]
                    };
                    break;                    
                default: 
                  alert(chartType + " tpye are not supported"); 
            }

            oChart._chartConfig = chartConfig;

            var oTheme = new ChartDarkTheme();
            
           Highcharts.setOptions(oTheme.theme);

            oChart._chartType = chartType;

            return oChart;
        }
    }
}


