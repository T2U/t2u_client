/// <reference path="../application.ts" />

'use strict';

module myApp.services {

    export class NavBarService implements IService {
		//	Custom Services
     	private oAuthenticationService: myApp.services.AuthenticationService;

     	constructor (private oService: myApp.services.AuthenticationService ) {
		 	//  Récupération du service
		  	this.oAuthenticationService = oService;
			this.oAuthenticationService._fillAuthData();
     	}

     	public getContentUrl(): string
        {
			if(myApp.services.AuthenticationService.authentication.isAuth){
				return 'views/navbarLogged.html';
			}
			else{
				return 'views/navbarUnlogged.html';
			}
        }
    }
}

// /!\ SUFFIX 'Directive' important /!\ http://stackoverflow.com/questions/19409017/angular-decorating-directives
myApp.registerService('NavBarService', ['authenticationService']);