#!/bin/bash
# @alandrieu 29-03-2015
# Team repo <https://bitbucket.org/T2U/t2u_client/overview>
# Private repo <https://github.com/alandrieu>

echo "Install dependencies" > T2U_Run.log
bower install && npm install 2>> T2U_Run.log

echo "Compile TypeScript src" >> T2U_Run.log
chmod +x compileTypeScript.sh 2>> T2U_Run.log
./compileTypeScript.sh 2>> T2U_Run.log

echo "Start Program HTTP-SERVER t2u_client" >> T2U_Run.log
npm start 2>> T2U_Run.log

echo "Stop Program HTTP-SERVER t2u_client" >> T2U_Run.log
pause