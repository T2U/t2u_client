Conditions G&eacute;n&eacute;ral d&rsquo;utilisation Pour T2U :
<br>Terms of Use Agreement For TU&sup2; .com and T2U API
<br>
<br>Conditions d'utilisation du Compte Client T2U et des services web
<br>
<br>Pr&eacute;ambule :
<br>
<br>T2U est une entreprise &agrave; but non lucratif les &eacute;quipes techniques travail sur le territoire fran&ccedil;ais, c&rsquo;est donc le droit fran&ccedil;ais qui s&rsquo;appliquera en cas de litige avec un tiers.
<br>
<br>Les pr&eacute;sentes Conditions d'utilisation du Compte Client T2U (ci-apr&egrave;s "Conditions d'utilisation") ont pour objet d'informer le Client T2U des conditions et modalit&eacute;s d'acc&egrave;s &agrave; ce Compte.
<br>
<br>Les Conditions d'utilisation sont compos&eacute;es de Conditions G&eacute;n&eacute;rales d'utilisation, compl&eacute;t&eacute;es par des Conditions Particuli&egrave;res d'utilisation applicables en fonction du segment de client&egrave;le auquel il appartient (Particuliers, Entreprises, Collectivit&eacute;s Locales).
<br>
<br>En cas de contradiction entre l'une quelconque des dispositions des Conditions G&eacute;n&eacute;rales et des Conditions Particuli&egrave;res, les termes des Conditions Particuli&egrave;res concern&eacute;es pr&eacute;vaudront.
<br>
<br>D&eacute;finitions :
<br>
<br>Les mots et expressions ci-apr&egrave;s, au singulier ou au pluriel, ont, dans le cadre des Conditions d'utilisations, la signification suivante :
<br>
<br>Client T2U
<br>D&eacute;signe tout consommateur final ayant cr&eacute;&eacute; un compte r&eacute;git par un CGU avec T2U.
<br>
<br>Conditions d'utilisation
<br>D&eacute;signent ensemble les Conditions G&eacute;n&eacute;rales et les Conditions Particuli&egrave;res d'utilisation du Compte Client T2U.
<br>
<br>Conseiller
<br>D&eacute;signe un conseiller susceptible d'assister par forum ou par voie &eacute;lectronique l'utilisateur dans la gestion de son Compte Client.
<br>
<br>Entreprise
<br>D&eacute;signe une personne morale publique ou priv&eacute;e qui exerce une activit&eacute; &eacute;conomique en utilisant du personnel, des locaux et des &eacute;quipements appropri&eacute;s.
<br>
<br>Compte Client
<br>D&eacute;signe le Compte d&eacute;di&eacute; &agrave; l'utilisateur duquel sont accessibles des informations mises &agrave; sa disposition par T2U (y compris ses Informations Personnelles).
<br>
<br>Formulaire d'Inscription
<br>D&eacute;signe le formulaire regroupant les Informations Personnelles de l'utilisateur auxquelles ils donnent acc&egrave;s dans le cadre de la cr&eacute;ation de son Compte Client.
<br>
<br>Identifiants
<br>D&eacute;signent l'adresse de courrier &eacute;lectronique ou un pseudonyme d&eacute;finit par l&rsquo;utilisateur et le mot de passe.
<br>
<br>Informations Personnelles
<br>D&eacute;signent l'ensemble des informations propres &agrave; l'utilisateur communiqu&eacute;s &agrave; T2U lors de son Inscription ou ult&eacute;rieurement.
<br>
<br>Partenaire
<br>D&eacute;signe toute Entreprise ayant conclu un contrat de partenariat avec T2U.
<br>
<br>Particulier
<br>D&eacute;signe une personne physique majeure. 
<br>Conditions G&eacute;n&eacute;rales d'utilisation du Compte Client T2U et des services web
<br>Article 1. Objet
<br>
<br>Les pr&eacute;sentes Conditions G&eacute;n&eacute;rales ont pour objet de d&eacute;finir les conditions d'acc&egrave;s et les modalit&eacute;s d'utilisation du Compte Client applicables &agrave; tout utilisateur.
<br>Article 2. Dur&eacute;e
<br>
<br>Les Conditions d'utilisation entrent en vigueur &agrave; compter de l'inscription de l'utilisateur et demeurent en vigueur entre T2U et l'utilisateur pour une dur&eacute;e ind&eacute;termin&eacute;e aussi longtemps que l'utilisateur dispose d'un Compte Client.
<br>Article 3. Inscription
<br>Article 3.1. Cr&eacute;ation de compte
<br>
<br>Toutes personnes morales a la facult&eacute; de s'inscrire pour cr&eacute;er son Compte Client.
<br>Article 3.2. Identifiants
<br>
<br>Les identifiants sont strictement confidentiels et r&eacute;serv&eacute;s &agrave; l'usage personnel de l'utilisateur.
<br>
<br>En cons&eacute;quence, l'utilisateur s'engage &agrave; ne pas les communiquer &agrave; un tiers de quelque mani&egrave;re que ce soit.
<br>
<br>En cas d'oubli du mot de passe, l'utilisateur perdra d&eacute;finitivement l&rsquo;acc&egrave;s &agrave; son compte et au donn&eacute;es personnel qu&rsquo;il renferme, en effet les donn&eacute;es des comptes sont prot&eacute;g&eacute;es par un algorithme de chiffrement fort et sans le mot de passe de l&rsquo;utilisateur celles-ci ne peuvent pas &ecirc;tre r&eacute;cup&eacute;r&eacute;es. En cas de perte, de vol ou de d&eacute;tournement des identifiants par un tiers, la responsabilit&eacute; de T2U ne pourra pas &ecirc;tre engag&eacute;e.
<br>
<br>L'utilisateur s'engage &agrave; avertir T2U, sans d&eacute;lai, d&egrave;s qu'il suspecte ou a connaissance de l'un des &eacute;v&egrave;nements mentionn&eacute;s &agrave; l'alin&eacute;a pr&eacute;c&eacute;dent, &agrave; l'adresse de courrier &eacute;lectronique du webmaster : t2u-support@t2u.com.
<br>
<br>Les cons&eacute;quences du non-respect par l'utilisateur de ses obligations sont pr&eacute;cis&eacute;es &agrave; l'article 4.3 des pr&eacute;sentes Conditions G&eacute;n&eacute;rales.
<br>
<br>Article 4. Droits et obligations de l'utilisateur
<br>Article 4.1. Droits de l'utilisateur
<br>
<br>L'utilisateur a la facult&eacute; d'acc&eacute;der &agrave; son Compte Client en renseignant ses identifiants. L'utilisateur peut &agrave; tout moment mettre &agrave; jour, modifier, corriger ses informations personnelles.
<br>
<br>Si l'utilisateur souhaite supprimer son Compte Client, ses Informations Personnelles seront supprim&eacute;es dans les conditions pr&eacute;cis&eacute;es &agrave; l'article 6.2 des pr&eacute;sentes Conditions G&eacute;n&eacute;rales.
<br>
<br>Article 4.2. Obligations de l'utilisateur
<br>
<br>D&rsquo;une mani&egrave;re g&eacute;n&eacute;rale, l&rsquo;utilisateur s'engage &agrave; :
<br>Respecter les termes des pr&eacute;sentes Conditions d'utilisation,
<br>Transmettre &agrave; T2U des informations exactes qu&rsquo;il a personnellement v&eacute;rifi&eacute;es,
<br>Prot&eacute;ger l&rsquo;acc&egrave;s &agrave; son Compte Client afin d&rsquo;&eacute;viter son utilisation par des tiers non autoris&eacute;s et avertir imm&eacute;diatement T2U, en utilisant les coordonn&eacute;es fournies &agrave; l&rsquo;article 3.2 des pr&eacute;sentes Conditions G&eacute;n&eacute;rales, en cas d'acc&egrave;s frauduleux, voire d&rsquo;utilisation frauduleuse,
<br>Ne pas porter atteinte aux droits de T2U et des tiers, notamment en utilisant toute machine, robot ou autre moyen, susceptible de modifier, r&eacute;acheminer, endommager, surcharger le Compte Client,
<br>S'abstenir, de toute utilisation d&eacute;tourn&eacute;e de donn&eacute;es personnelles de quiconque, et d'une mani&egrave;re g&eacute;n&eacute;rale, de tout acte susceptible de porter atteinte &agrave; la vie priv&eacute;e ou &agrave; la r&eacute;putation des personnes.
<br>
<br>Article 4.3. Restriction / Suspension / Suppression
<br>
<br>En cas de manquement par l'utilisateur aux pr&eacute;sentes Conditions d'Utilisation, T2U se r&eacute;serve le droit de proc&eacute;der &agrave; une restriction, une suspension ou une suppression de l'acc&egrave;s au Compte Client de l'utilisateur, suivant une mise en demeure adress&eacute;e par courrier &eacute;lectronique.
<br>
<br>Article 5. Droits et obligations de T2U
<br>
<br>T2U s'efforcera de maintenir accessible &agrave; l'utilisateur son Compte Client.
<br>
<br>Cependant, T2U pourra en restreindre, suspendre ou en supprimer l'acc&egrave;s, notamment pour des raisons de maintenance et de mise &agrave; jour ou pour toute autre raison technique imp&eacute;rieuse.
<br>
<br>T2U s'engage &agrave; pr&eacute;venir, dans la mesure du possible, l'utilisateur d'&eacute;ventuelles coupures, suspensions et, plus g&eacute;n&eacute;ralement, de toute op&eacute;ration de maintenance ou de mise &agrave; jour envisag&eacute;e concernant le Compte Client.
<br>
<br>T2U peut restreindre, refuser, suspendre ou supprimer un acc&egrave;s au Compte Client en cas de suspicion d'intervention d'un tiers non autoris&eacute;.
<br>
<br>T2U peut modifier, suspendre ou migrer l'exploitation du site Internet en tout ou partie.
<br>
<br>Article 6. Informations Personnelles
<br>
<br>T2U s&rsquo;engage &agrave; respecter les lois et r&egrave;glements en vigueur relatifs &agrave; la protection de la vie priv&eacute;e.
<br>Article 6.1. D&eacute;claration CNIL
<br>
<br>En conformit&eacute; avec les dispositions de la loi n&deg;78-17 du 6 janvier 1978 relative &agrave; l'Informatique, aux fichiers et aux libert&eacute;s, le traitement automatis&eacute; de donn&eacute;es personnelles mis en &#339;uvre dans le cadre de la mise en place des Comptes Client a fait l'objet d'une d&eacute;claration aupr&egrave;s de la Commission Nationale de l'Informatique et des Libert&eacute;s (CNIL) n&deg;9999999
<br>
<br>L'utilisateur a &eacute;t&eacute; inform&eacute; qu'il dispose, conform&eacute;ment &agrave; l'article 27 de la loi pr&eacute;cit&eacute;e, d'un droit individuel d'acc&egrave;s, de rectification et de suppression portant sur les donn&eacute;es personnelles qui le concernent. Ce droit peut &ecirc;tre exerc&eacute; :
<br>
<br>- Soit par courrier &eacute;lectronique adress&eacute; au webmaster : info.t2u@t2u.com 
<br>- Soit en adressant un courrier &agrave; l&rsquo;adresse suivante :
<br>
<br>T2U &shy; Place Bellecour
<br>69000 Lyon
<br>France
<br>T&eacute;l : + 33 2 48 94 28 00
<br>Fax : + 33 2 48 94 28 01
<br>
<br>
<br>
<br>
<br>Article 6.2. Conservation
<br>
<br>Les Informations Personnelles communiqu&eacute;es par l&rsquo;utilisateur &agrave; l&rsquo;occasion de la cr&eacute;ation de son Compte Client sont confidentielles.
<br>
<br>Communiquer des Informations Personnelles &agrave; T2U dans le cadre de son Compte Client, est une d&eacute;marche enti&egrave;rement volontaire de la part de l'utilisateur. Sauf si elle y a &eacute;t&eacute; autoris&eacute;e par l'utilisateur, T2U s&rsquo;engage &agrave; ne pas divulguer &agrave; des tiers les Informations Personnelles relatives &agrave; l&rsquo;utilisateur. T2U conserve les Informations Personnelles de l'utilisateur aussi longtemps que ce dernier dispose d'un Compte Client.
<br>
<br>T2U est susceptible de conserver les Informations Personnelles de l'utilisateur apr&egrave;s suppression de son Compte Client. Aucune Information Personnelle ne sera conserv&eacute;e par T2U autres que celles n&eacute;cessaires au respect par T2U de ses obligations l&eacute;gales et r&eacute;glementaires.
<br>
<br>Article 6.3. Traitement
<br>
<br>En fournissant &agrave; T2U des Informations Personnelles lors de la cr&eacute;ation de son Compte Client, ou en modifiant les dites Informations post&eacute;rieurement, l'utilisateur accepte express&eacute;ment les dispositions des pr&eacute;sentes Conditions d'Utilisation et accepte le traitement de ses Informations Personnelles par T2U.
<br>
<br>L'utilisateur a le droit de retirer son consentement &agrave; la collecte et au traitement de ses Informations Personnelles par T2U &agrave; tout moment en en informant T2U, voire en supprimant son Compte Client.
<br>
<br>Article 6.4. S&eacute;curit&eacute;
<br>
<br>T2U dispose d'acc&egrave;s s&eacute;curis&eacute; afin de prot&eacute;ger les Informations Personnelles que l'utilisateur fournit, dans le respect de la r&eacute;glementation en vigueur.
<br>
<br>Pour prot&eacute;ger les Informations Confidentielles de l'utilisateur stock&eacute;es sur ses serveurs, T2U effectue des contr&ocirc;les r&eacute;guliers, par le billet d&rsquo;audits, de ses syst&egrave;mes pour identifier les attaques &eacute;ventuelles.
<br>
<br>Toutefois, T2U ne peut pas garantir la s&eacute;curit&eacute; des donn&eacute;es qui lui sont transmises.
<br>
<br>T2U ne garantit pas que les donn&eacute;es ne soient pas d&eacute;truites du fait des sauvegardes de gestion, techniques ou physiques.
<br>
<br>L'utilisateur est responsable de la protection et de la s&eacute;curit&eacute; de ses idenntifiants. En cas de non-respect de cet engagement, il s'expose &agrave; ce que T2U exerce l'une des facult&eacute;s qui lui est ouverte en application de l'article 4.3 des pr&eacute;sentes Conditions G&eacute;n&eacute;rales.
<br>
<br>Article 6.5. Communication
<br>
<br>T2U communique avec l'utilisateur par messages sur le forum et courrier &eacute;lectronique, par publication sur le site Internet dans une banni&egrave;re d&rsquo;information.
<br>
<br>Ces messages lui seront envoy&eacute;s selon ses pr&eacute;f&eacute;rences indiqu&eacute;es dans son Compte Client.
<br>
<br>Article 6.6. Divulgation
<br>
<br>T2U peut &ecirc;tre amen&eacute;e &agrave; divulguer des donn&eacute;es de l'utilisateur si la loi l'exige ou sur requ&ecirc;te judiciaire ou dans le cadre de toute proc&eacute;dure l&eacute;gale.
<br>
<br>T2U peut contester de telles demandes lorsqu'elle estime, &agrave; sa discr&eacute;tion, que ces demandes sont trop larges, vagues ou ne sont pas d&ucirc;ment autoris&eacute;es.
<br>
<br>Article 6.7. Donn&eacute;es de connexion, de navigation et de localisation
<br>
<br>T2U enregistre les donn&eacute;es lorsque l'utilisateur se connecte dans son Compte Client.
<br>
<br>Lorsque l'utilisateur se connecte sur son Compte Client, ou si un cookie T2U sur son appareil l'identifie, ses donn&eacute;es de connexion, telles que son adresse IP, seront associ&eacute;es &agrave; son Compte Client.
<br>
<br>Si l'utilisateur ne souhaite pas que T2U re&ccedil;oive des donn&eacute;es de g&eacute;olocalisation provenant de services tiers ou d'appareils &eacute;quip&eacute;s de GPS, T2U rappelle que la plupart des appareils mobiles permettent d'emp&ecirc;cher cet envoi de donn&eacute;es.
<br>
<br>Article 6.8. Partenariat
<br>
<br>T2U peut recourir aux services de Partenaires afin d'optimiser le Compte Client.
<br>
<br>Ces Partenaires ont un acc&egrave;s illimit&eacute; aux donn&eacute;es des utilisateurs.
<br>
<br>Cet acc&egrave;s ne leur permet que d'effectuer des t&acirc;ches au nom de T2U, et ils sont tenus envers T2U de ne pas divulguer ces donn&eacute;es ni de les utiliser &agrave; d'autres fins que celles autoris&eacute;es par T2U.
<br>
<br>Des enqu&ecirc;tes et des sondages peuvent &ecirc;tre r&eacute;alis&eacute;s par T2U ou des Partenaires.
<br>
<br>Les Partenaires conduisant l'enqu&ecirc;te ou le sondage solliciteront explicitement son autorisation aux fins d'utilisation de toutes donn&eacute;es personnelles identifiables r&eacute;pondant aux objectifs du sondage ou de l'enqu&ecirc;te.
<br>
<br>Article 6.9. Cookies
<br>
<br>T2U utilise les cookies et technologies similaires, pour l'aider &agrave; reconna&icirc;tre les utilisateurs, am&eacute;liorer leur exp&eacute;rience sur T2U, augmenter la s&eacute;curit&eacute;, mesurer l'usage de ses services, et diffuser des publicit&eacute;s.
<br>
<br>L'utilisateur peut contr&ocirc;ler les cookies par le biais de ses pr&eacute;f&eacute;rences de navigateur et d'autres outils.
<br>
<br>En acceptant les pr&eacute;sentes Conditions d'Utilisation, l'utilisateur consent au placement de cookies et de balises dans son navigateur.
<br>
<br>Article 7. Propri&eacute;t&eacute; Intellectuelle
<br>
<br>Toutes les informations ou documents contenus sur l'ensemble des Comptes Clients sont soit la propri&eacute;t&eacute; de T2U, soit font l'objet d'un droit d'utilisation, d'exploitation et de reproduction au b&eacute;n&eacute;fice de T2U. Ces &eacute;l&eacute;ments sont soumis &agrave; la l&eacute;gislation prot&eacute;geant le droit d'auteur. Par cons&eacute;quent, aucune licence, ni aucun autre droit sauf celui de consulter les informations, n'est conf&eacute;r&eacute; &agrave; quiconque au regard des droits de la propri&eacute;t&eacute; intellectuelle.
<br>
<br>Toute reproduction, repr&eacute;sentation, modification, publication, transmission, d&eacute;naturation, totale ou partielle des pages Internet ou de son contenu, par quelque proc&eacute;d&eacute; que ce soit, et sur quelque support que ce soit est interdite.
<br>
<br>Toute exploitation non autoris&eacute;e des pages Internet ou de son contenu, des informations qui y sont divulgu&eacute;es engagerait la responsabilit&eacute; de l'utilisateur et constituerait une contrefa&ccedil;on sanctionn&eacute;e par les articles L. 335-2 et suivants du Code de la propri&eacute;t&eacute; intellectuelle.
<br>
<br>Il en est de m&ecirc;me des bases de donn&eacute;es qui sont prot&eacute;g&eacute;es par les dispositions de la loi du 1er juillet 1998 portant transposition dans le Code de la propri&eacute;t&eacute; intellectuelle de la Directive europ&eacute;enne du 11 mars 1996 relative &agrave; la protection juridique des bases de donn&eacute;es. A ce titre, toute reproduction ou extraction engagerait la responsabilit&eacute; de l'utilisateur.
<br>
<br>La marque et le logo de T2U sont des marques d&eacute;pos&eacute;es.
<br>
<br>Toute reproduction ou repr&eacute;sentation totale ou partielle de cette marque et du logo, seules ou int&eacute;gr&eacute;es &agrave; d'autres &eacute;l&eacute;ments, sans l'autorisation expresse et pr&eacute;alable de T2U est prohib&eacute;e, et engagerait la responsabilit&eacute; de l'utilisateur au sens des articles L. 713-2 et L. 713-3 du Code de la propri&eacute;t&eacute; intellectuelle.
<br>
<br>L'utilisateur s'engage, sous peine de voir sa responsabilit&eacute; civile et/ou p&eacute;nale engag&eacute;e, &agrave; ne pas utiliser son Compte Client pour transmettre par quelque proc&eacute;d&eacute; que ce soit (email ou autres), tout contenu incluant des programmes, des codes, des virus destin&eacute;s &agrave; d&eacute;truire ou limiter les fonctionnalit&eacute;s des pages Internet.
<br>
<br>T2U se r&eacute;serve la facult&eacute;, dans le cadre de ses services interactifs, de supprimer imm&eacute;diatement et sans mise en demeure pr&eacute;alable, tout contenu de quelque nature que ce soit, et notamment tout message, texte, image, graphique, qui contreviendrait &agrave; la r&eacute;glementation en vigueur.
<br>
<br>Dans l'hypoth&egrave;se o&ugrave; un utilisateur souhaiterait utiliser un des contenus du site Internet (texte, image...), il s'engage &agrave; requ&eacute;rir l'autorisation pr&eacute;alable et &eacute;crite de T2U, en &eacute;crivant &agrave; l'adresse indiqu&eacute;e &agrave; l'article 6.1 des pr&eacute;sentes Conditions G&eacute;n&eacute;rales.
<br>
<br>Article 8. Responsabilit&eacute;
<br>
<br>T2U n&rsquo;&eacute;met aucune garantie de performance et de disponibilit&eacute; du Compte Client.
<br>
<br>Dans tous les cas, T2U ne r&eacute;pond que des dommages mat&eacute;riels directs et pr&eacute;visibles subis par l&rsquo;utilisateur et caus&eacute;s par tout manquement &agrave; ses obligations d&eacute;coulant des pr&eacute;sentes Conditions d'Utilisation ou de la loi, dans la limite d&rsquo;un montant de 100 000 euros.
<br>
<br>T2U ne peut en aucun cas &ecirc;tre tenue pour responsable d&rsquo;un quelconque dommage immat&eacute;riel direct ou d&rsquo;un dommage indirect, qu'il soit mat&eacute;riel ou immat&eacute;riel, pr&eacute;visible ou impr&eacute;visible.
<br>
<br>Les limitations de responsabilit&eacute; &eacute;nonc&eacute;es au pr&eacute;sent article sont inapplicables en cas de dol ou de faute lourde de T2U, ou si l&rsquo;utilisateur est un consommateur et qu&rsquo;elles contreviennent aux r&egrave;gles prot&eacute;geant ce dernier. 
<br>
<br>Article 9. Dispositions g&eacute;n&eacute;rales
<br>9.1. Convention de preuve
<br>
<br>L&rsquo;utilisateur reconna&icirc;t, dans ses relations contractuelles avec T2U, la validit&eacute; et la force probante des courriers &eacute;lectroniques &eacute;chang&eacute;s entre eux.
<br>
<br>9.2. Force majeure
<br>
<br>T2U ne sera pas tenue pour responsable, ou consid&eacute;r&eacute;e comme ayant failli aux dispositions des pr&eacute;sentes Conditions d'Utilisation, pour tout retard ou inex&eacute;cution lorsque la cause du retard ou de l'inex&eacute;cution r&eacute;sulte d&rsquo;un cas de force majeure tel que d&eacute;fini par la jurisprudence des tribunaux fran&ccedil;ais.
<br>
<br>9.3. Nullit&eacute; partielle
<br>
<br>Toutes dispositions des Conditions d'Utilisation, qui viendraient &agrave; &ecirc;tre d&eacute;clar&eacute;es nulles ou illicites par un juge comp&eacute;tent seront priv&eacute;es d'effet.
<br>
<br>La nullit&eacute; d&rsquo;une telle disposition ne saurait porter atteinte aux autres dispositions des Conditions d'utilisation, ni affecter leur validit&eacute; dans leur ensemble ou leurs effets juridiques.
<br>
<br>9.4. Loi applicable
<br>
<br>Les pr&eacute;sentes conditions d'utilisation sont soumises au droit fran&ccedil;ais en cas de litige concernant leur formation, leur interpr&eacute;tation et leur ex&eacute;cution.
<br>
<br>9.5. Comp&eacute;tence juridictionnelle
<br>
<br>A d&eacute;faut de r&egrave;glement amiable et &agrave; l'exception des cas pr&eacute;vus par l'article 48 du code de proc&eacute;dure civile, en cas de litige relatif &agrave; la formation, l&rsquo;interpr&eacute;tation ou l&rsquo;ex&eacute;cution des conditions d&rsquo;utilisation, T2U et les utilisateurs donnent comp&eacute;tence expresse et exclusive au Tribunal de Grande Instance de Paris, nonobstant la pluralit&eacute; de d&eacute;fendeurs, ou d&rsquo;action en r&eacute;f&eacute;r&eacute;, ou d'appel en garantie, ou encore de demande de mesure conservatoire.
<br>
<br>9.6. Modification
<br>
<br>T2U se r&eacute;serve le droit de modifier &agrave; tout moment tout ou partie des pr&eacute;sentes Conditions d'Utilisation.
<br>
<br>Dans le cas de modifications substantielles de la mani&egrave;re de traiter, g&eacute;rer, collecter et/ou stocker les Informations Personnelles de l'utilisateur, T2U enverra un courrier &eacute;lectronique &agrave; chaque utilisateur. L'utilisateur est fortement invit&eacute; &agrave; prendre connaissance des modifications avec attention. Si l'utilisateur accepte ces modifications, il lui suffit de continuer &agrave; utiliser son Compte Client.
<br>
<br>Si l'utilisateur a des objections vis-&agrave;-vis des modifications apport&eacute;es aux Conditions d'utilisation, il peut supprimer son Compte Client.
<br>
<br>9.7. Contact
<br>
<br>Pour toute question, assistance ou r&eacute;clamation concernant le Compte Client, l'utilisateur est invit&eacute; &agrave; se r&eacute;f&eacute;rer aux "FAQ" accessibles sur le site web.
<br>
<br>II. Conditions Particuli&egrave;res d'utilisation du Compte Client T2U
<br>
<br>Les pr&eacute;sentes conditions particuli&egrave;res pr&eacute;cisent les conditions applicables en fonction du segment de client&egrave;le concern&eacute;.
<br>
<br>II.1. Conditions Particuli&egrave;res applicables aux Particuliers
<br>Article 1. Conditions et modalit&eacute;s d'inscription
<br>
<br>Pour s'inscrire, le Particulier renseigne un pseudonyme et son adresse de courrier &eacute;lectronique.
<br>
<br>T2U ne demandera en aucun cas la communication de pi&egrave;ces justificatives, comme la copie d'une carte nationale d'identit&eacute; et d'une facture de fourniture d'&eacute;lectricit&eacute;.
<br>
<br>II.2. Conditions Particuli&egrave;res applicables aux Entreprises
<br>Article 1. Conditions et modalit&eacute;s d'inscription
<br>
<br>Pour s'inscrire, la personne habilit&eacute;e par l'entreprise compl&egrave;te un formulaire d'inscription r&eacute;cup&eacute;rer apr&egrave;s une demande explicite aupr&egrave;s de nos services en indiquant la d&eacute;nomination sociale et une adresse de courrier &eacute;lectronique.
<br>
<br>T2U pourra demander la communication de pi&egrave;ces justificatives, comme la copie d'une attestation d&ucirc;ment &eacute;tablie et sign&eacute;e par le repr&eacute;sentant l&eacute;gal de l'entreprise, sur papier &agrave; en-t&ecirc;te et avec le cachet de l'entreprise.
<br>
<br>Article 2. Compte administrateur / comptes utilisateurs
<br>
<br>Les identifiants sont strictement personnel &agrave; l'utilisateur qui est d&eacute;sign&eacute; comme personne habilit&eacute;e &agrave; acc&eacute;der au Compte Client.
<br>
<br>T2U d&eacute;cline toute responsabilit&eacute; quant &agrave; la gestion interne &agrave; l'entreprise des identifiants permettant l'acc&egrave;s au Compte Client.
<br>