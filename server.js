#!/bin/env node

/**
 * Module dependencies
 */
var express = require('express'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  morgan = require('morgan'),
  http = require('http'),
  compress = require('compression'),
  path = require('path');

var DEFAULT_PORT =  process.env.OPENSHIFT_NODEJS_PORT || 80;
var DEFAULT_IP = process.env.OPENSHIFT_NODEJS_IP;

var app = module.exports = express();

/**
 * Configuration
 */
// all environments
app.set('port', DEFAULT_PORT);
app.use(express.static(path.join(__dirname, 'app')));
app.use(compress());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(methodOverride());

var env = process.env.NODE_ENV || 'development';

// development only
if (env === 'development') {
  // TODO
}

// production only
if (env === 'production') {
  // TODO
}

app.get('/*', function (req, res, next) {
  if (req.url.indexOf("/bower_components/") === 0 ||
      req.url.indexOf("/css/") === 0) {
    res.setHeader("Cache-Control", "public, max-age=2592000");
    res.setHeader("Expires", new Date(Date.now() + 2592000000).toUTCString());
  }
  next();
});

/**
 * Start Server
 */
http.createServer(app).listen(app.get('port'), DEFAULT_IP, function () {
  console.log('Express server listening on port ' + app.get('port'));
});