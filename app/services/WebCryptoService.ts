/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />

'use strict';

module myApp.services {

    export class WebCryptoService extends HttpHandlerService {

        private _localStorageService: angular.local.storage.ILocalStorageService;

        private cntx: myApp.bean.Context;

        private oCrypto: myApp.util.WebCrypto;

        constructor(private $http: ng.IHttpService,
					private localStorageService: angular.local.storage.ILocalStorageService,
					private $q: ng.IQService) {

			super($http);

            this.handlerUrl = "/crypto";

            this.cntx = myApp.bean.Context.currentContext;
            this._localStorageService = localStorageService;

            if(this._localStorageService.get('WebCrypto') == null ||
               this._localStorageService.get('WebCrypto') == {} ){

				this.oCrypto = new myApp.util.WebCrypto($q);

				this.oCrypto.keyGeneration().then( (key: any) => {
						this._localStorageService.set('WebCrypto', this.oCrypto);
						console.log(this.oCrypto);

						if(this.oCrypto.currentKeyPair != null)
            				this.oCrypto.encryptMessage("TOTO test 1 2 3 4 5", this.oCrypto.currentKeyPair.publicKey).then( (result: any) => {
            					console.log(new Uint8Array(result));	
								this.oCrypto.decryptMessage(result);
            				},
			            	(err) => {
			                    console.error(err);
			                })
	            	},
	            	(err) => {
	                    console.error(err);
	                }
            	)
        	}
            else{
            	var webCrypto: any = this._localStorageService.get('WebCrypto');

				this.oCrypto = webCrypto;
            }            
        }
    }
}

myApp.registerService('WebCryptoService', ['$http', 'localStorageService', '$q']);
