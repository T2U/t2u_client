/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />

'use strict';

/*
Documentation :

- Angular Storage : https://github.com/grevory/angular-local-storage/blob/master/demo/demo-app.js
- https://github.com/borisyankov/DefinitelyTyped/blob/master/angular-local-storage/angular-local-storage.d.ts
- Auth template example : https://github.com/tjoudeh/AngularJSAuthentication

*/
module myApp.services {

    export class AuthenticationService extends HttpHandlerService {
        private serviceBase: string;

        private authServiceFactory;

        private localIQService : ng.IQService;

        private cntx: myApp.bean.Context;

        private static _authentication = {
            isAuth: false,
            email: ""
        };

        static get authentication(): any {
            return this._authentication;
        }
        /*static set authentication(value: _authentication) {
            this._authentication = value;
        }*/

        private token = 
        {
            data: String,
            issuedUtc: String,
            expiresUtc: String,
            email: String
        }

        /* le Token pour réaliser les appels*/
        private static _currentToken: myApp.bean.Token;
        static get currentToken(): myApp.bean.Token {
            return this._currentToken;
        }
        
        // Stockage des informations de l'utilisateur
        public oStorageService: angular.local.storage.ILocalStorageService;

        constructor(private $http: ng.IHttpService, private $q: ng.IQService, private localStorageService: angular.local.storage.ILocalStorageService )
        {
            super( $http );

            this.localIQService = $q;

            //console.log("AuthenticationService:HTTP:" + String($http != null));
            //console.log("AuthenticationService:localStorageService:" + String(localStorageService != null));

            this.authServiceFactory = {};

            this.oStorageService = localStorageService;
        }

        /**
         *   Sauvegarder un nouveau Account
         */
        _saveRegistration(registration): ng.IPromise< any > {

            this._logOut();

            this.handlerUrl = "/account/register";

            return this.usePostHandler( registration );
        }

        /**
         *   Deconnexion de l'utilisateur entraine la suppression du Token.
         */
        _logOut(): void {
            this.oStorageService.remove('authorizationData');

            myApp.services.AuthenticationService._authentication.isAuth = false;
            myApp.services.AuthenticationService._authentication.email = "";

            // Effacer le contexte courant 
            myApp.bean.Context.clear();
        }

        /**
         *    Tentative de connexion de l'utilisateur et récupération d'un Token pour réaliser les appels au web service
         */
        _login(loginData): ng.IPromise< any > {

            this.handlerUrl = "/token";

            var deferred = this.localIQService.defer();

            this.usePostHandler(loginData).then( ( response ) => {
                //console.log(response);

                //var result = angular.fromJson(response);
                //console.log(result);

                for (var i = 0; i < response.length; i++) {
                    this.token.data = response[i].data;
                    this.token.issuedUtc = response[i].issuedUtc;
                    this.token.expiresUtc = response[i].expiresUtc;
                    this.token.email = loginData.email;
                    break;
                }

                //var result = angular.fromJson(response);
                //console.log(this.token);

                this.oStorageService.set('authorizationData', this.token);

                // myApp.services.AuthenticationService.
                //myApp.services.AuthenticationService._authentication.isAuth = true;
                //myApp.services.AuthenticationService._authentication.email = loginData.email;
                
                //console.log(myApp.services.AuthenticationService.authentication);

                this._fillAuthData();
                
                deferred.resolve(response);
            },
              // Error Should also return a string to be compatible with the return type
              (reason) => {
                this._logOut();
                deferred.reject(reason);
                }
            )

            return deferred.promise;
        }

        /**
         * Refresh static variable.
         */
        public _fillAuthData(): void {

            var tokenData: any = this.oStorageService.get('authorizationData');

            //console.log(tokenData);

            if (tokenData) {
                myApp.services.AuthenticationService._authentication.isAuth = true;
                myApp.services.AuthenticationService._authentication.email = tokenData.email;

                myApp.services.AuthenticationService._currentToken = new myApp.bean.Token(tokenData.data, tokenData.issuedUtc, tokenData.expiresUtc)

                this.cntx = new myApp.bean.Context(myApp.services.AuthenticationService._currentToken, true, tokenData.email);
            }

            //console.log(myApp.services.AuthenticationService.authentication);
        }
    }
}

// 
myApp.registerService('AuthenticationService', ['$http', '$q' ,'localStorageService']);
