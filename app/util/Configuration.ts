/// <reference path="../application.ts" />

'use strict';

module myApp.services {
	export class Configuration implements IService {

		private static _instance: myApp.services.Configuration;
		static get instance(): myApp.services.Configuration {
        	return this._instance;
    	}
    	static set instance(value: myApp.services.Configuration) {
        	this._instance = value;
    	}

		/* Activation du HTTPS */
		private _SSL_enabled: boolean = false;
    	get SSL_enabled():boolean {
        	return this._SSL_enabled;
    	}
    	set SSL_enabled(value:boolean) {
        	this._SSL_enabled = value;
    	}

    	/* Port pour les appels HTTP */
    	private _port: number = 8000;
    	get Port():number {
        	return this._port;
    	}
    	set Port(value:number) {
        	this._port = value;
    	}

    	/* Nom du service pour les appels HTTP */
    	private _webServiceName: String = "";
    	get WebServiceName():String {
        	return this._webServiceName;
    	}
    	set WebServiceName(value:String) {
        	this._webServiceName = value;
    	}

    	/* Nom de l'hote pour les appels HTTP */
    	private _hostName: String = "";
    	get HostName():String {
        	return this._hostName;
    	}
    	set HostName(value:String) {
        	this._hostName = value;
    	}

        /* Nom de l'hote pour les appels BI */
        private _hostNameBI: String = "";
        get HostNameBI():String {
            return this._hostNameBI;
        }
        set HostNameBI(value:String) {
            this._hostNameBI = value;
        }        

		constructor(private thehttpService:ng.IHttpService, private pFicConf: String) {
            var configFilePath: String = pFicConf || 'config/config.json';

            thehttpService.get(configFilePath.toString()).then(( response: any ) => {
                // console.log(response);
                this.loadConfigurationFromString(response.data);
            });

			myApp.services.Configuration.instance = this;
		}

        /*
            Charge la configuration du client web depuis un String JSON
        */
        loadConfigurationFromString(jSonData: String) : void{
            //console.log(jSonData);
            
            var result: any = jSonData;
            var configObject: any;
            
            result = angular.fromJson(result);

            // #tag=configuration CHANGE HERE TO LOAD DEVELOPPMENT OR PRODUCTION CONFIGURATION
            //configObject = result.configuration_dev;
            if(location.protocol == "http")
                configObject = result.configuration_prod;
            else
                configObject = result.configuration_prod_secure;

            this._hostName = configObject.hote;
            this._port = configObject.port;
            this._webServiceName = configObject.nomservice;
            this.SSL_enabled = configObject.ssl_enabled;
            this._hostNameBI = configObject.bi_server;
        }        
	}
}

myApp.registerService('Configuration', ['$http', String]);