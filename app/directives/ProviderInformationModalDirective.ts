/// <reference path="../application.ts" />
/// <reference path='../libs/types/angularjs/angular.d.ts'/>

'use strict';

/**
 * Directive pour la visualisation des informations sur un Fournisseurs de service.
 */
module myApp.directives {

    export interface IProviderInformationModalDirectiveScope extends myApp.controllers.ISupportedProviderControllerScope
    {
        title: string;
    }

    export interface IProviderInformationModalDirectiveAttributes extends ng.IAttributes
    {
        title: string;
        visible: any;
    }

    export class ProviderInformationModalDirective implements ng.IDirective {
        public restrict = 'ACE';

    	compile(templateElement: ng.IAugmentedJQuery) {
        	return {
            	pre: this.link
        	};
    	}

        public templateUrl = 'views/directives/ProviderInformation.html';

    	private currentScope: IProviderInformationModalDirectiveScope;

        public transclude = true;
        public replace = true;
        public scope = true;

        // Détermine si la directive est visible ou non
        private _isVisible: boolean;
        public get isVisible():boolean {
            return this._isVisible;
        }

        private myElement: ng.IAugmentedJQuery;

	    static instance(): ng.IDirective {
	        return new ProviderInformationModalDirective();
    	}

        // Attribut de la Directive
        private _title: String;
        public get title():String {
            return this._title;
        }    
        public set title(sTitle: String) {
            this._title = sTitle;
        }

        public description: string;
        public imageBase64: string;

		public link: (scope: IProviderInformationModalDirectiveScope, element: ng.IAugmentedJQuery, attrs: IProviderInformationModalDirectiveAttributes) => void;

    	constructor()
        {
            this.InitiateDirective();

            this.link = (scopeDir: IProviderInformationModalDirectiveScope, element: ng.IAugmentedJQuery, attrs: IProviderInformationModalDirectiveAttributes) =>{	
                // Injection de la référence du controller dans le $scope
                this.currentScope = scopeDir;

                this.currentScope.title = attrs.title;
                this.myElement = element;

                var oController;
                oController = this.currentScope.vm;

                if (oController != null) {
                    oController.providerInformationDirective = this;
                }
                
                // Fermeture de la fenêtre si un click à l'extérieur de celle-ci
                element.on("click", event => {
                    var isClickedElementChildOfPopup = element
                        .find(event.target)
                        .length > 0;

                    if (isClickedElementChildOfPopup)
                        return;

                    this.hide();
                });

                this.compile(element);
        	};


            //console.log(this);
        }

        /**
         * Initialiser les attributs de la directive
         */
        private InitiateDirective():void
        {
            this._isVisible = false;
        }

        /**
         * Afficher la directive
         */
        public show():void{
          this.myElement.show();
          this._isVisible = true;
        }

        /**
         * Cacher la directive
         */
        public hide():void{
             this.myElement.hide();
             this._isVisible = false;
        }

        /**
         * Valider les modification
         */
        public submit():void{
        }
    }
}

myApp.registerDirective('ProviderInformationModalDirective', []);