/// <reference path="../application.ts" />

'use strict';

module myApp.bean {

    export class Token {

        private data: String;
        private issuedUtc: String;
        private expiresUtc: String;

        constructor(private _data: String, private _issuedUtc: String, private _expiresUtc: String  )
        {
            this.data = _data;
            this.issuedUtc = _issuedUtc;
            this.expiresUtc = _expiresUtc;
        }
    }
}