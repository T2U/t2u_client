/// <reference path="../application.ts" />

'use strict';

module myApp.bean {

    export class ChartTheme {

        protected _theme: any;
        get theme(): any {
            return this._theme;
        }

        constructor()
        {
        }
    }
}