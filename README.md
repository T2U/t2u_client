# TU² Client, application Release-1.1.1

Copyright (C) 2014-2015, TU² (ISITECH Student team).

[![AppVeyor status](https://ci.appveyor.com/api/projects/status/6f69e20p6480b25f?svg=true)](https://ci.appveyor.com/project/alandrieu/t2u-client)
[![wercker status](https://app.wercker.com/status/43bd1d75b7d6fa061a9799c48063ba76/s "wercker status")](https://app.wercker.com/project/bykey/43bd1d75b7d6fa061a9799c48063ba76)
[![Codeship status](https://codeship.com/projects/81eb6970-c251-0132-1769-4610f64a79b0/status?branch=master)](https://codeship.com/projects/73738)
------------------------------------------------------------------------
------------------------------------------------------------------------

This software is currently maintained by,

* @Alandrieu
* @Gappy

A part of TU² Team (ISITECH Student team).

If you have problems, your best bet is to post an issue to the [list](https://bitbucket.org/T2U/t2u_client/issues?status=new&status=open).

Introduction
------------
TU² Client, is a WebClient for T2U student team powered by NodeJS, AngularJS and TypeScript.

More information on [Wiki](https://bitbucket.org/T2U/t2u_client/wiki/Home)

Installation
------------

### Install Requirements from program source files

* NodeJS : v0.12.2 or higher (https://nodejs.org/)
* Npm : v2.5.1 or higher (https://www.npmjs.com/)
* Bower : v1.4.1 or higher (http://bower.io/ or npm install -g bower)
* TypeScript compiler : v1.5.0 or higher (http://www.typescriptlang.org/ or npm i -g typescript)

### Builds and Run

On your Linux or Windows operating system run the script ~/t2u_client/run.sh.

```sh
./run.sh
```

Quick Start
-----------

On ~/t2u_client/ make :

```sh
bower install && npm start
```

License
------------

The MIT License

Copyright (c) 2014-2015 T2U Student team, composed of Alexis Landrieu and Guillaume Appy. https://bitbucket.org/T2U

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

## CI Builds

### AppVeyor
[![AppVeyor status](https://ci.appveyor.com/api/projects/status/6f69e20p6480b25f?svg=true)](https://ci.appveyor.com/project/alandrieu/t2u-client)

### Wercker
[![wercker status](https://app.wercker.com/status/43bd1d75b7d6fa061a9799c48063ba76/m "wercker status")](https://app.wercker.com/project/bykey/43bd1d75b7d6fa061a9799c48063ba76)

### Codeship
[![Codeship status](https://codeship.com/projects/81eb6970-c251-0132-1769-4610f64a79b0/status?branch=master)](https://codeship.com/projects/73738)