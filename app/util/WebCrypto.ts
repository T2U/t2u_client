/// <reference path="../application.ts" />
'use strict';

module myApp.util {
    //
    // MS documentation : https://msdn.microsoft.com/fr-fr/library/dn265046(v=vs.85).aspx
    //
    export class WebCrypto {

        private _localStorageService: angular.local.storage.ILocalStorageService;

        public currentKeyPair: any;

        private localIQService: ng.IQService;

        // Encryption engine
        private crypto:  any;

        constructor(private $q: ng.IQService) {

            //this._localStorageService = localStorageService;
            this.crypto = window.crypto; // || window.msCrypto; // pour IE 11
            
            //this.keyGeneration();
            this.localIQService = $q;
        }

        /**
         * RSASSA-PKCS1-v1_5 - generateKey (https://github.com/diafygi/webcrypto-examples)
         */
        public keyGeneration(): ng.IPromise < any > {
            var deferred = this.localIQService.defer();

            this.crypto.subtle.generateKey({
                    name: "RSA-OAEP",
                    modulusLength: 4096, //can be 1024, 2048, or 4096
                    publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
                    hash: {
                        name: "SHA-512"
                    }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
                },
                false, //whether the key is extractable (i.e. can be used in exportKey)
                ["encrypt", "decrypt"] //must contain both "encrypt" and "decrypt"
            )
            .then((key: any) => {

                    //returns a keypair object
                    /*console.log(key);
                    console.log(key.publicKey);
                    console.log(key.privateKey);*/

                    this.currentKeyPair = key;

                    deferred.resolve(key);
                },
                // Error Should also return a string to be compatible with the return type
                (err) => {
                    deferred.reject(err);
                }
            )

            return deferred.promise;
        }

        /**
         *RSASSA-PKCS1-v1_5 - importKey
         */
        /*  public importKey(): void{
              this.crypto.subtle.importKey(
                  "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
                  {   //this is an example jwk key, other key types are Uint8Array objects
                      kty: "RSA",
                      e: "AQAB",
                      n: "vGO3eU16ag9zRkJ4AK8ZUZrjbtp5xWK0LyFMNT8933evJoHeczexMUzSiXaLrEFSyQZortk81zJH3y41MBO_UFDO_X0crAquNrkjZDrf9Scc5-MdxlWU2Jl7Gc4Z18AC9aNibWVmXhgvHYkEoFdLCFG-2Sq-qIyW4KFkjan05IE",
                      alg: "RS256",
                      ext: true,
                  },
                  {   //these are the algorithm options
                      name: "RSASSA-PKCS1-v1_5",
                      hash: {name: "SHA-512"}, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
                  },
                  false, //whether the key is extractable (i.e. can be used in exportKey)
                  ["verify"] //"verify" for public key import, "sign" for private key imports
              )
              .then(function(publicKey){
                  //returns a publicKey (or privateKey if you are importing a private key)
                  console.log(publicKey);
              })
              .catch(function(err){
                  console.error(err);
              });
          }*/

        public exportPublicKey(): void {
            this.crypto.subtle.exportKey(
                    "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
                    this.currentKeyPair.publicKey //can be a publicKey or privateKey, as long as extractable was true
                )
                .then(function(keydata) {
                    //returns the exported key data
                    console.log(keydata);
                })
                .catch(function(err) {
                    console.error(err);
                });
        }

        public exportPrivateKey(): void {
            this.crypto.subtle.exportKey(
                    "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
                    this.currentKeyPair.privateKey //can be a publicKey or privateKey, as long as extractable was true
                )
                .then(function(keydata) {
                    //returns the exported key data
                    console.log(keydata);
                })
                .catch(function(err) {
                    console.error(err);
                });
        }

        public signMessage(data: any): any {
            this.crypto.subtle.sign({
                        name: "RSASSA-PKCS1-v1_5",
                    },
                    this.currentKeyPair.privateKey, //from generateKey or importKey above
                    data //ArrayBuffer of data you want to sign
                )
                .then(function(signature) {
                    //returns an ArrayBuffer containing the signature
                    console.log(new Uint8Array(signature));
                })
                .catch(function(err) {
                    console.error(err);
                });
        }

        public verifyMessage(data: any, publicKey: any, signature: any): void {
            this.crypto.subtle.verify({
                    name: "RSASSA-PKCS1-v1_5",
                },
                publicKey, //from generateKey or importKey above
                signature, //ArrayBuffer of the signature
                data //ArrayBuffer of the data
            ).then((isvalid: any) => {
                    //returns a boolean on whether the signature is true or not
                    console.log(isvalid);
                },
                (err) => {
                    console.error(err);
                }
            )
        }

        public encryptMessage(data: any, publicKey: any): ng.IPromise < any > {
            var deferred = this.localIQService.defer();
            //var dataBuffer = new ArrayBuffer(data);

            this.crypto.subtle.encrypt({
                    name: "RSA-OAEP",
                    //label: Uint8Array([...]) //optional
                },
                publicKey, //from generateKey or importKey above
                this.str2ab(data) //ArrayBuffer of data you want to encrypt
            ).then((encrypted: any) => {
                    //returns an ArrayBuffer containing the encrypted data
                    //console.log(new Uint8Array(encrypted));
                    //console.log(encrypted);
                    //console.log(this.ab2str(encrypted));

                    deferred.resolve(encrypted);
                },
                (err) => {
                    //console.error(err);
                    deferred.reject(err);
                }
            )

            return deferred.promise;
        }

        public decryptMessage(data: any): void {
            this.crypto.subtle.decrypt({
                    name: "RSA-OAEP",
                    //label: Uint8Array([...]) //optional
                },
                this.currentKeyPair.privateKey, //from generateKey or importKey above
                data //ArrayBuffer of the data
            ).then((decrypted: any) => {
                    //returns an ArrayBuffer containing the decrypted data
                    console.log(new Uint8Array(decrypted));
                    console.log(this.ab2str(decrypted));
                    console.log(decrypted);
                },
                (err) => {
                    //console.error(err);
                    console.error(err);
                }
            )
        }

        private ab2str(buf): any {
            return String.fromCharCode.apply(null, new Uint16Array(buf));
        }

        private str2ab(str): any {
            var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
            var bufView = new Uint16Array(buf);
            for (var i = 0, strLen = str.length; i < strLen; i++) {
                bufView[i] = str.charCodeAt(i);
            }
            return buf;
        }
    }
}