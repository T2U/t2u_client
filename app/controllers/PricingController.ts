/// <reference path="../application.ts" />
/// <reference path="../services/View2Service.ts" />

'use strict';

module myApp.controllers {

	export interface IPricingControllerScope extends IControllerScope {
	}

	export class PricingController implements IController {

		//	Custom Services
		//private oPricingService: myApp.services.PricingService;
		private scope: any;

		constructor (private $scope: IPricingControllerScope) {
		  	// Injection de la référence du controller dans le $scope
		  	$scope.vm = this;

		  	this.scope = $scope;
     	}

     	public test(item: any): boolean
     	{
        	return true;
     	}
 	}
}

// Remember to pass all the services used by the constructor of the Controller.
// WARNING PricingService but PricingService doesn't work
myApp.registerController('PricingController', ['$scope']);