/// <reference path="../application.ts" />
/// <reference path='../libs/types/angularjs/angular.d.ts'/>

'use strict';

module myApp.directives {

    export interface IMyDirectiveScope extends myApp.controllers.INavBarControllerScope
    {
        name: string;
    }

    export class MyDirective implements ng.IDirective {

        public restrict = 'AC';

    	compile(templateElement: ng.IAugmentedJQuery) {
        	return {
            	pre: this.link
        	};
    	}
    	
    	public template = '<div ng-include="vm.getContentUrl()"></div>';
  
    	public scope: IMyDirectiveScope;

        public controller = "myApp.controllers.NavBarController";
	    static instance():ng.IDirective {
	        return new MyDirective();
    	}

		public link: (scope: IMyDirectiveScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes) => void;

    	constructor()
        {            
            this.link = (scope: IMyDirectiveScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes) =>{	
            	/*handle all your linking requirements here*/
        	};
        }
    }
}

myApp.registerDirective('MyDirective', []);
