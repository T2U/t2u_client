/// <reference path="../application.ts" />

'use strict';

module myApp.bean {

    export class Context {

    	public static _currentContext: Context;
        static get currentContext(): Context {
            return this._currentContext;
        }

        private _token: Token;
        get token(): Token {
            return this._token;
        }

        private _isAuth: boolean;
        get isAuth(): boolean {
            if (this._isAuth != null)
                return this._isAuth;
            else
                return false;
        }

        /*private _account = { email: "", username: "", uuid: 0 };*/
        private _account: Account;

        get account(): Account {
            return this._account;
        }

        constructor( private newtoken: Token, private newisAuth: boolean, private newemail: string )
        {
            this._token = newtoken;
			this._isAuth = newisAuth;
			//this._email = newemail;
            this._account = new Account(newemail, "undefined", -1);

            Context._currentContext = this;
        }

        /**
         * Effacer le contexte courant
         */
        public static clear(): void
        {
            Context._currentContext = null;
        }
    }
}