/// <reference path="../application.ts" />

'use strict';

module myApp.controllers {

  	export interface INavBarControllerScope extends IControllerScope {
  	}

    export class NavBarController implements IController {
    	private oNavBarService: myApp.services.NavBarService;

    	private scope: any;

      // Configuration de l'application
      protected _config: myApp.services.Configuration;
      public get config():myApp.services.Configuration {
          return this._config;
      }

      // Le context du controlleur
      protected _cntx: myApp.bean.Context;
      public get cntx():myApp.bean.Context {
          return this._cntx;
      }

      constructor (private $scope: INavBarControllerScope, private oNavService: myApp.services.NavBarService) {
        // Injection de la référence du controller dans le $scope
  	    $scope.vm = this;

        this.scope = $scope;

  	    // Récupération du service de naviguation
  	    this.oNavBarService = oNavService;

        // Récupération du context courrant    
        this._cntx = myApp.bean.Context.currentContext;

        this._config = myApp.services.Configuration.instance;
        //if(myApp.services.Configuration.instance == null)
          //var configurationObject = new myApp.services.Configuration(this.httpService, "config/config.json");

        console.log(this._config);
      }

      /**
       * Retourner l'url pour choisir une vue connecté / deconnecté
       */
      public getContentUrl(): string {
      	return this.oNavBarService.getContentUrl();
      }
    }
}

// Remember to pass all the services used by the constructor of the Controller.
myApp.registerController('NavBarController', ['$scope', 'navBarService']);
