/// <reference path="_all.ts" />

'use strict';

// Create and register modules
var modules = ['myApp.controllers','myApp.directives', 'myApp.filters', 'myApp.services'];
modules.forEach((module) => angular.module(module, ['ngRoute', 'highcharts-ng', 'LocalStorageModule', 'ui.bootstrap']));
angular.module('myApp', modules);

// Url routing $httpProvider: ng.IHttpProvider
angular.module('myApp').config(['$routeProvider',
    function routes($routeProvider: ng.route.IRouteProvider) {
        $routeProvider
        .when('/', {
            templateUrl: 'views/Home.html',
            controller: 'myApp.controllers.MyController'
            })
        .when('/dashboard', {
            templateUrl: 'views/DashBoard.html',
            controller: 'myApp.controllers.DashBoardController'
            })
        .when('/login', {
            templateUrl: 'views/Login.html',
            controller: 'myApp.controllers.LoginController'
            })
        .when('/forgotpassword', {
            templateUrl: 'views/ForgotPassword.html'
            })        
        .when('/signup', {
            templateUrl: 'views/Signup.html',
            controller: 'myApp.controllers.SignUpController'
            })
        .when('/supportedprovider', {
            templateUrl: 'views/ProvidersList.html',
            controller: 'myApp.controllers.SupportedProviderController'
            })
        .when('/pricing', {
            templateUrl: 'views/Pricing.html',
            controller: 'myApp.controllers.PricingController'
            })
        .when('/howitworks', {
            templateUrl: 'views/HowItWorks.html'
            })
        .when('/support', {
            templateUrl: 'views/Support.html',
            controller: 'myApp.controllers.MyController'
            })
        .when('/userprofile', {
            templateUrl: 'views/UserProfile.html',
            controller: 'myApp.controllers.UserProfileController'
            }) 
        .when('/privacypolicy', {
            templateUrl: 'views/PrivacyPolicy.html'
            })
        .when('/termsconditions', {
            templateUrl: 'views/TermsConditions.html'
            })
        .when('/legalnotice', {
            templateUrl: 'views/LegalNotice.html'
            })                          
        .otherwise({
            redirectTo: '/'
            });
    }]);

// Création du module de stockage d'informaiton
angular.module('myApp').config(function (localStorageServiceProvider: angular.local.storage.ILocalStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('myApp')
    .setStorageType('sessionStorage')
    .setNotify(true, true);
});

module myApp {
    export module controllers {}
    export module directives {}
    export module filters {}
    export module services {}

    export interface IController {}
    /*export interface IDirective {
        restrict: string;
        link($scope: ng.IScope, element: JQuery, attrs: ng.IAttributes): any;
    }*/
    export interface IFilter {
        filter (input: any, ...args: any[]): any;
    }
    export interface IService {}

    /**
     * Register new controller.
     *
     * @param className
     * @param services
     */
     export function registerController (className: string, services = []) {
        var controller = 'myApp.controllers.' + className;
        services.push(myApp.controllers[className]);
        angular.module('myApp.controllers').controller(controller, services);
    }

    /**
     * Register new filter.
     *
     * @param className
     * @param services
     */
     export function registerFilter (className: string, services = []) {
        var filter = className.toLowerCase();
        services.push(() => (new myApp.filters[className]()).filter);
        angular.module('myApp.filters').filter(filter, services);
    }

    /**
     * Register new directive.
     *
     * @param className
     * @param services
     */
     export function registerDirective (className: string, services = []) {
        var directive = className[0].toLowerCase() + className.slice(1);
        services.push(() => new myApp.directives[className]());
        //services.push(myApp.directives[className]);
        angular.module('myApp.directives').directive(directive, services);
    }

    /**
     * Register new service.
     *
     * @param className
     * @param services
     */
    export function registerService (className: string, services = []) {
        var service = className[0].toLowerCase() + className.slice(1);
        services.push(myApp.services[className]);
        angular.module('myApp.services').service(service, services);

        /*services.push(() => new myApp.services[className]());
        angular.module('myApp.services').factory(service, services);*/
    }
}