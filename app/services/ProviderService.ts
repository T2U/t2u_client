/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />

'use strict';

/**
 * Service pour la récupération des informations sur les fournisseurs de service
 */
module myApp.services {

    export class ProviderService extends HttpHandlerService {

        private cntx: myApp.bean.Context;

        constructor( $http: ng.IHttpService )
        {
            super( $http );

            this.handlerUrl = "/provider";

            this.cntx = myApp.bean.Context.currentContext;
        }

        /* Retourner une liste d'information sur la base de donnée. */
        public getProvider(data: any, token: any): ng.IPromise< any >
        {
            if (token != null) {
                var header = {
                    "headers": {
                        "Token": angular.toJson(token)
                    }
                };
            }

            return this.useGetHandler( header );
        }       
    }
}

myApp.registerService('ProviderService', ['$http']);
