/// <reference path="../application.ts" />
/// <reference path='../libs/types/angularjs/angular.d.ts'/>

'use strict';

module myApp.directives {

    export interface INavBarDirectiveScope extends myApp.controllers.INavBarControllerScope
    {
        name: string;
    }

    export class NavBarDirective implements ng.IDirective {

        public restrict = 'AC';

    	compile(templateElement: ng.IAugmentedJQuery) {
        	return {
            	pre: this.link
        	};
    	}
    	
    	public template = '<div ng-include="vm.getContentUrl()"></div>';
  
    	public scope: INavBarDirectiveScope;

        public controller = "myApp.controllers.NavBarController";

	    static instance():ng.IDirective {
	        return new NavBarDirective();
    	}

		public link: (scope: INavBarDirectiveScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes) => void;

    	constructor()
        {
            this.link = (scope: INavBarDirectiveScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes) =>{	
            	/*handle all your linking requirements here*/

            	//console.log(scope);
            	/*scope.navBarDirective = this;
            	var oNavBarController = <myApp.controllers.NavBarController>scope.vm;

            	this.templateUrl = oNavBarController.getContentUrl();
            	this.compile(element);*/
        	};;
        }
    }
}

myApp.registerDirective('NavBarDirective', []);
