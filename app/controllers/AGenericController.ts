/// <reference path="../application.ts" />

'use strict';

module myApp.controllers {
  /**
   * Interface pour la définition générique d'un scope
   */
  export interface IControllerScope extends ng.IScope {
    vm: IController;
  }

  /**
   * Class Abstract pour la définition des méthodes et des Attributs générique au Controlleur
   */
  export class AGenericController implements IController {
      // Le context du controlleur
      public _cntx: myApp.bean.Context;
 /*     public get cntx():myApp.bean.Context {
          return this._cntx;
      }
      public set cntx(oCntx: myApp.bean.Context) {
          this._cntx = oCntx;
      }*/

      constructor() {
        //console.log("AGenericController constructor");
      }
  }


}

/*myApp.registerController('AController', []);*/