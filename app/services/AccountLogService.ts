/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />

'use strict';

/**
 * This class permit to get AccountLog information for target user.
 */
module myApp.services {

    export class AccountLogService extends HttpHandlerService {

        private cntx: myApp.bean.Context;

        constructor( $http: ng.IHttpService )
        {
            super( $http );

            this.handlerUrl = "/accountlog";

            this.cntx = myApp.bean.Context.currentContext;
        }

        /**
         * Retourner une liste d'information sur la base de donnée. 
         */
        public getAccountLog(data: any, token: any): ng.IPromise< any >
        {
            if (token != null) {
                var header = {
                    "headers": {
                        "Token": angular.toJson(token)
                    }
                };
            }
            console.log(data);
            this.handlerUrlParam = "?criteria=" + encodeURIComponent(angular.toJson(data));

            console.log(this.handlerUrl);

            return this.useGetHandler(header);
        }          
    }
}

myApp.registerService('AccountLogService', ['$http']);
