/// <reference path="../application.ts" />
/// <reference path="../services/View2Service.ts" />

'use strict';

module myApp.controllers {

	export interface IView2ControllerScope extends IControllerScope {
	}

	export class View2Controller implements IController {

		//	Custom Services
		private oView2Service: myApp.services.View2Service;
		private scope: any;

		private isAuth: boolean;

		constructor (private $scope: IView2ControllerScope, private oService:  myApp.services.View2Service) {
		  // Injection de la référence du controller dans le $scope
		  $scope.vm = this;

		  //  Récupération du service
		  this.oView2Service = oService;

		  this.scope = $scope;

      this.scope.message = "TEST "; //oService.simpleGetCall();

		  this.isAuth = myApp.services.AuthenticationService.authentication.isAuth;
     	}

     	public swap(item: any): boolean
     	{
     		if(item != null)
     		{
				var id = item.currentTarget.id; // 345

  				$('#'+ id).find('.card').addClass('flipped');
        		return false;
  			}
     	}


     	public revertswap(item: any): boolean
     	{
     		if(item != null)
     		{
				var id = item.currentTarget.id; // 345

  				$('#'+ id).find('.card').removeClass('flipped');
        		return false;
  			}
     	}
 	}
}

// Remember to pass all the services used by the constructor of the Controller.
// WARNING view2Service but View2Service doesn't work
myApp.registerController('View2Controller', ['$scope', 'view2Service']);