/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />

'use strict';

module myApp.services {

    export class GroupService extends HttpHandlerService {

        private cntx: myApp.bean.Context;

        constructor( $http: ng.IHttpService )
        {
            super( $http );

            this.handlerUrl = "/group";

            this.cntx = myApp.bean.Context.currentContext;
        }

        /* Retourner une liste d'information sur la base de donnée. */
        getGroup(token): ng.IPromise< any >
        {
              var header = {
                      "headers": {
                        "Token":  angular.toJson(token)
                      }
                    };

            return this.useGetHandler( header );
        }
    }
}

myApp.registerService('GroupService', ['$http']);
