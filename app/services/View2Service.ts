/// <reference path="../application.ts" />

'use strict';

module myApp.services {

    export class View2Service extends HttpHandlerService {
    	public http: ng.IHttpService;

        constructor (private $http: ng.IHttpService ) {
        	/*console.log("View2Service:HTTP:" + $http);*/
			super( $http );

            /*console.log("View2Service:handlerUrl:" + this.handlerUrl.toString());*/
        }

        simpleGetCall () : string {
            return 'View2Service ';
        }
    }
}

myApp.registerService('View2Service', ['$http']);