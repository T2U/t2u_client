/// <reference path="../application.ts" />
// /// <reference path="../services/ChartService.ts" />

'use strict';

module myApp.controllers {
  
  export interface IDashBoardControllerScope extends IControllerScope {
  }

  export class DashBoardController implements IController {

    // Le context du controlleur
    protected _cntx: myApp.bean.Context;
    public get cntx():myApp.bean.Context {
        return this._cntx;
    }

		//	Custom Services
		private oChartService: myApp.services.ChartService;

    //private scope: ng.IScope;
    private scope: any;
    private http: ng.IHttpService;

    // Liste des Charts
    private chartObjectArray = new Array();

    // Chart D'exemple
    private pieChartExample: myApp.bean.ChartObject;
    private singleLineSeriesChartExample: myApp.bean.ChartObject;
    private basicAreaChartExample: myApp.bean.ChartObject;

    private chartResult: any;   
    private chart = {
          account: { email: '' }
    };

		constructor (private $scope: IDashBoardControllerScope, private oService:  myApp.services.ChartService) {
		  // Injection de la référence du controller dans le $scope
      $scope.vm = this;

		  //  Récupération du service
      this.oChartService = oService;

      this.scope = $scope;

      // Récupération du context courrant
      this._cntx = myApp.bean.Context.currentContext;

      // Chargement des Graphiques
      this.loadingAllCharts();

      // Exemple :
      this.pieChartExample = myApp.bean.ChartObject.getNewChart('pie');

      this.singleLineSeriesChartExample = myApp.bean.ChartObject.getNewChart('single_line_series');

      this.basicAreaChartExample = myApp.bean.ChartObject.getNewChart('basic_area');
      
    }

  /**
   * Chargement de tous les Charts du Client
   */
    public loadingAllCharts(){
      var oChart: myApp.bean.ChartObject;

      if (this.oChartService != null) {
       
        // Récupération des charts du Client connecté
        this.chart.account.email = this.cntx.account.email;

        this.oChartService.getChart(this.chart, this.cntx.token).then(
          (data) => {
            
            //console.log(data); 

            this.chartResult = data;
             
            // Pour chaque Chart retourné
            for(let val of this.chartResult){
              oChart = myApp.bean.ChartObject.getNewChart(myApp.bean.ChartObject.getRealChartName(val.chartType));
              
              /* Tableau HighChartJS */
              oChart.chartConfig.subtitle.text = val.description;

              for(let val2 of val.enteteDocumentList){
                // Ajouter les données à oChart depuis val2
                this.addSeries2(val2, oChart);

                // Ajouter le Chart a la liste
                this.chartObjectArray.push(oChart);
              }
            }
          },
          (reason) => {
            //console.log(reason);
            return ''; 
          }
        )
      }
    }

    public simpleGetCall(lsucces: boolean) {
      this.oChartService.getExtractbdd().then( ( data ) => {
        this.addSeries(data)
      });
    }

    /*
      Ajoute une série au tableau HightChartJS
    */
    public addSeries(jSonData: String) : void{
      // Tableau de variable
      var rnd = [];

      /*if( jSonData.length != 0){*/
      var empty: string = "";
      var jString: any = jSonData;

      if (jSonData === empty) {
        for (var i = 0; i < 10; i++) {
          rnd.push(Math.floor(Math.random() * 20) + 1)
        }
      }else{
        // Récupération de la liste des événements dans la variable {jSonData}
        var result = angular.fromJson(jString);
        // alert(result);

        // Récupération de la durée de chaque événement et l'ajouter au HighChart
        for (var i = 0; i < result.length; i++) {
          rnd.push(result[i].montant);
        }
      }

      this.pieChartExample.chartConfig.series.push({data: rnd});
      this.singleLineSeriesChartExample.chartConfig.series.push({data: rnd});
    }

    /**
     * Version 2 avec un Chart
     */
    public addSeries2(enteteDocument: any, oChart: myApp.bean.ChartObject) : void{
      // Tableau de variable
      var rnd = [];

      /*if( jSonData.length != 0){*/
      var empty: string = "";
      
      if (enteteDocument != null) {
        for(let val of enteteDocument.lineDocumentList){
                 rnd.push(val.valeur01);
        }
      }

      // Ajouter la Série au Chart
      oChart.chartConfig.series.push({data: rnd, name : enteteDocument.description + "," + enteteDocument.updatedString});
    }    

  /**
   * Change le Mode d'un Chart
   */
    public swapChartType(oChartObject: myApp.bean.ChartObject): void{
      //console.log(oChartObject);

      if (oChartObject.chartType == 'bar') {
        if (oChartObject.chartConfig.options.chart.type === 'line') {
          oChartObject.chartConfig.options.chart.type = 'bar';
        } else {
          oChartObject.chartConfig.options.chart.type = 'line';
          //this.chartConfig.options.chart.zoomType = "x"; // 
        }
      }
    }    
  }
}

// Remember to pass all the services used by the constructor of the Controller.
// WARNING chartService but ChartService doesn't work
myApp.registerController('DashBoardController', ['$scope', 'chartService']);