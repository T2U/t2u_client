﻿REM COMPILE TS
REM @alandrieu 29-03-2015
REM "Team repo <https://bitbucket.org/T2U/t2u_client/overview>"
REM "Private repo <https://github.com/alandrieu>"
tsc --sourcemap --target ES5 app/_all.ts --out app/out.js