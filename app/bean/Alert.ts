/// <reference path="../application.ts" />

'use strict';

module myApp.bean {

    export class Alert {

        private _type: string;
        get type(): string {
            return this._type;
        }

        private _msg: string;
        get msg(): string {
            return this._msg;
        }

        constructor(private type_: string, private msg_: string)
        {
            this._type = type_;
            this._msg = msg_;
        }
    }
}