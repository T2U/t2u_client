/// <reference path="../application.ts" />
/// <reference path="../services/HttpHandlerService.ts" />

'use strict';

module myApp.services {

    export class ChartService extends HttpHandlerService {
        private meaningOfLife = 9999;

        constructor( $http: ng.IHttpService )
        {
            super( $http );

            this.handlerUrl = "/extractbdd";

        }

        /* Retourner une liste de Graphiques/Chart. */
        public getChart(data: any, token: myApp.bean.Token): ng.IPromise< any >
        {
            this.handlerUrl = "/chart";

            if (token != null) {
                var header = {
                    "headers": {
                        "Token": angular.toJson(token)
                    }
                };
            }

            if(data != null)
                this.handlerUrlParam = "?criteria=" + encodeURIComponent(angular.toJson(data));

            // console.log(this.handlerUrl);
            // console.log(this.handlerUrlParam);

            return this.useGetHandler(header);
        }

         /* Retourner une liste d'information sur la base de donnée. */
        getExtractbdd(): ng.IPromise< any >
        {
            this.handlerUrl = "/extractbdd";
            var config: any = {};
            return this.useGetHandler( config );
        }
    }
}

myApp.registerService('ChartService', ['$http']);
